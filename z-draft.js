// const f1 = () => {
// 	throw { code: 10, message: 'f1', stack: new Error('').stack };
// };

// try {
// 	f1();
// } catch (e) {
// 	console.log(e);
// }

// const testObj = {
// 	first: 'first',
// 	second: {
// 		third: 'third',
// 		fourth: {
// 			fifth: 'fifth',
// 			sixth: {
// 				seventh: 'seventh',
// 			},
// 		},
// 	},
// };

// console.dir(testObj, { showHidden: true, depth: Infinity });

import { wait } from './src/utils/wait.js';

const run = async () => {
	await wait(5000);
	console.log('run', 5000);
};

run();
