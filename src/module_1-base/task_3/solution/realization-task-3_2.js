import { USERS } from '../../../data/users.js';

export const realizationTask_3_2 = () => {
	return USERS.filter((user) => user.age > 18 && user.city === 'Kazan').sort(
		(a, b) => a.name.localeCompare(b.name)
	);
};

console.log(realizationTask_3_2());
