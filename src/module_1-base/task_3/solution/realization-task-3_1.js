import { USERS } from "../../../data/users.js";

export const realizationTask_3_1 = () => {
	return USERS.filter((user) => user.age > 18 && user.city === 'Kazan').sort(
		(a, b) => {
			if (a.name < b.name) {
				return -1;
			}

			if (a.name > b.name) {
				return 1;
			}

			return 0;
		}
	);
};

console.log(realizationTask_3_1());
