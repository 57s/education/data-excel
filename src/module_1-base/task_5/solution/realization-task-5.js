import { ORDERS2020 } from '../../../data/orders2020.js';

export const realizationTask_5 = () => {
	const users = [];

	ORDERS2020.forEach((order) => {
		//  Найти пользователя в результате
		const user = users.find((user) => user.id === order.idUser);

		// Если пользователь уже есть в результате
		// пушим в заказы еще один
		if (user) {
			user.orders.push(order.idProduct);
		} else {
			// Если пользователя в результате нет
			// Создаем и пушим
			const newUser = {
				id: order.idUser,
				orders: [order.idProduct],
			};

			users.push(newUser);
		}
	});

	return users;
};

console.log(realizationTask_5());
