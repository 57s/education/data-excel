# Task 4

## Задание

- Объединить данные из `ORDERS2020` и `ORDERS2025`
- Вернуть только `book`

## Пример

```js
[
	{ id: 'o_2025_35', idUser: 'u00006', idProduct: 'p00031', count: 1 },
	{ id: 'o_2025_37', idUser: 'u00008', idProduct: 'p00032', count: 1 },
	{ id: 'o_2025_39', idUser: 'u00009', idProduct: 'p00033', count: 1 },
	{ id: 'o_2025_40', idUser: 'u00010', idProduct: 'p00034', count: 1 },
	{ id: 'o_2025_41', idUser: 'u00001', idProduct: 'p00035', count: 1 },
	{ id: 'o_2025_42', idUser: 'u00002', idProduct: 'p00036', count: 1 },
	{ id: 'o_2025_43', idUser: 'u00003', idProduct: 'p00037', count: 1 },
	{ id: 'o_2025_44', idUser: 'u00004', idProduct: 'p00038', count: 1 },
	{ id: 'o_2025_45', idUser: 'u00005', idProduct: 'p00039', count: 1 },
	{ id: 'o_2025_46', idUser: 'u00006', idProduct: 'p00040', count: 1 },
];
```
