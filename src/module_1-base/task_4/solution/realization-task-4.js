import { ORDERS2020 } from '../../../data/orders2020.js';
import { ORDERS2025 } from '../../../data/orders2025.js';
import { PRODUCTS } from '../../../data/products.js';

export const realizationTask_4 = () => {
	// объединяем массивы и фильтруем
	return [...ORDERS2020, ...ORDERS2025].filter((order) => {
		// Найти продукт в `PRODUCTS`
		// Что бы посмотреть его тип
		const product = PRODUCTS.find(
			(productElement) => productElement.id === order.idProduct
		);

		// Если тип книга то оставляем заказ
		if (product.type === 'book') return true;

		return false;
	});
};

console.log(realizationTask_4());

// Функция переданная в `.filter()`
// Должна вернуть true для элемента который оставить
// И false для элемента которого нужно исключить
