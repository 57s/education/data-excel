import { ORDERS2020 } from '../../../data/orders2020.js';
import { ORDERS2025 } from '../../../data/orders2025.js';

export const realizationTask_2 = () => {
	return [...ORDERS2020, ...ORDERS2025].sort((a, b) => b.count - a.count);
};

console.log(realizationTask_2());
