import { ORDERS2020 } from '../../../data/orders2020.js';
import { ORDERS2025 } from '../../../data/orders2025.js';
import { PRODUCTS } from '../../../data/products.js';
import { USERS } from '../../../data/users.js';

export const realizationTask_8 = () => {
	const result = [];

	// Объединение массивов для дальнейшего перебора
	[...ORDERS2020, ...ORDERS2025].forEach((orderElement) => {
		// Найти продукт текущего заказа
		const productData = PRODUCTS.find(
			(productItem) => productItem.id === orderElement.idProduct
		);

		// Найти пользователя текущего заказа
		const userData = USERS.find(
			(userItem) => userItem.id === orderElement.idUser
		);

		// Найти пользователя в результате
		const userResult = result.find((userItem) => userItem.id === userData.id);

		// Инициализация нового пользователя
		const newUser = {
			id: userData.id,
			name: userData.name,
			food: 0,
			gadget: 0,
			book: 0,
		};

		if (userResult) {
			// Если в результате есть пользователь

			switch (productData.type) {
				case 'food': {
					userResult.food++;
					break;
				}

				case 'gadget': {
					userResult.gadget++;
					break;
				}

				case 'book': {
					userResult.book++;
					break;
				}
			}
		} else {
			// Если в результате нет пользователь
			switch (productData.type) {
				case 'food': {
					newUser.food++;
					break;
				}

				case 'gadget': {
					newUser.gadget++;
					break;
				}

				case 'book': {
					newUser.book++;
					break;
				}
			}

			result.push(newUser);
		}
	});

	return result;
};

console.log(realizationTask_8());
