# Task 4

## Задание

Вернуть массив пользователей из 'Moscow', которые заказывали "gadget"

- `id` - идентификатор пользователя
- `name` - Имя пользователя
- `productName` - Название продуктов

- Данные берем из `USERS`, `ORDERS2020`,`ORDERS2025`, `PRODUCTS`
- Результат отсортировать по возрасту пользователя

## Пример

```js
[
	{
		id: 'u00002',
		name: 'Vitalik',
		productName: ['phone', 'steamdeck'],
	},
	{
		id: 'u00001',
		name: 'Kate',
		productName: ['player'],
	},
];
```
