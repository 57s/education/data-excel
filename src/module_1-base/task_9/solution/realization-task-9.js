import { ORDERS2020 } from '../../../data/orders2020.js';
import { ORDERS2025 } from '../../../data/orders2025.js';
import { PRODUCTS } from '../../../data/products.js';
import { USERS } from '../../../data/users.js';

export const realizationTask_9 = () => {
	const result = [];

	// Объединение массивов для дальнейшего перебора
	[...ORDERS2020, ...ORDERS2025].forEach((orderElement) => {
		// Найти продукт текущего заказа
		const productData = PRODUCTS.find(
			(productItem) => productItem.id === orderElement.idProduct
		);

		// Найти пользователя текущего заказа
		const userData = USERS.find(
			(userItem) => userItem.id === orderElement.idUser
		);

		const resultData = result.find(
			(userData) => userData.id === orderElement.idUser
		);

		// Проверка типа товара и города пользователя
		if (productData.type === 'gadget' && userData.city === 'Moscow') {
			// Если есть в результате то добавляем еще одно название
			if (resultData) {
				resultData.productName.push(productData.name);
			} else {
				// Если нет в результате то создаем и пушим
				// Инициализация нового пользователя
				const newUser = {
					id: userData.id,
					name: userData.name,
					age: userData.age,
					productName: [productData.name],
				};

				result.push(newUser);
			}
		}
	});

	// Сортировка и очистка лишних данных
	return result
		.sort((a, b) => b.age - a.age)
		.map((item) => ({
			id: item.id,
			name: item.name,
			productName: item.productName,
		}));
};

console.log(realizationTask_9());
