import { ORDERS2020 } from '../../../data/orders2020.js';
import { ORDERS2025 } from '../../../data/orders2025.js';
import { PRODUCTS } from '../../../data/products.js';

export const realizationTask_6 = () => {
	const resultProduct = [];

	function collectDate(array) {
		array.forEach((order) => {
			// Найти продукт в результате
			const product = resultProduct.find((item) => item.id === order.idProduct);

			if (product) {
				// Если продукт был найден то складываем их количество
				product.count = product.count + order.count;
			} else {
				// Если продукт не был найден то создаем новый продукт
				const newProduct = {
					id: order.idProduct,
					count: order.count,
					// Имя ищем в массиве PRODUCTS.
					// Возвращается объект, у него забираем "name"
					name: PRODUCTS.find((item) => item.id === order.idProduct).name,
				};

				// Пушим сформированный новый продукт
				resultProduct.push(newProduct);
			}
		});
	}

	// Обрабатываем данных из двух массивов
	collectDate(ORDERS2020);
	collectDate(ORDERS2025);

	// Сортируем результат
	const sortProduct = resultProduct.sort(
		(productLeft, productRight) => productRight.count - productLeft.count
	);

	return sortProduct;
};

console.log(realizationTask_6());
