# Task 3

## Задание

Сформировать объект(отчет):

- `totalSold` - Всего продано штук
- `totalPrice` - Сумма всех проданных товаров
- `topProducts` - отсортированный массив из 10 самых продаваем товаров
- `topUsers` - отсортированный массив из 10 самых активных пользователей

Данные берем из `ORDERS2020`

## Пример

```js
{
  totalSold: 238,
  totalPrice: 1665624,
  topProducts: [
    { id: 'p00002', count: 28 },
    { id: 'p00001', count: 18 },
    { id: 'p00005', count: 18 },
    { id: 'p00010', count: 17 },
    { id: 'p00007', count: 17 },
    { id: 'p00017', count: 13 },
    { id: 'p00004', count: 13 },
    { id: 'p00011', count: 12 },
    { id: 'p00014', count: 12 },
    { id: 'p00012', count: 12 }
  ],
  topUsers: [
    { id: 'u00001', orders: 12 },
    { id: 'u00005', orders: 12 },
    { id: 'u00008', orders: 12 },
    { id: 'u00002', orders: 10 },
    { id: 'u00003', orders: 10 },
    { id: 'u00009', orders: 10 },
    { id: 'u00004', orders: 9 },
    { id: 'u00010', orders: 8 },
    { id: 'u00007', orders: 7 },
    { id: 'u00006', orders: 7 }
  ]
}
```
