import { ORDERS2020 } from '../../../data/orders2020.js';
import { ORDERS2025 } from '../../../data/orders2025.js';
import { PRODUCTS } from '../../../data/products.js';

export const realizationTask_7_1 = () => {
	const report = {};

	// Количество заказов в`ORDERS2020`
	const totalSoldORDERS2020 = ORDERS2020.reduce(
		(totalSold, order) => (totalSold += order.count),
		0
	);

	// Количество заказов в`ORDERS2025`
	const totalSoldORDERS2025 = ORDERS2025.reduce(
		(totalSold, order) => (totalSold += order.count),
		0
	);

	// Сумма заказов в`ORDERS2020`
	const totalPriceORDERS2020 = ORDERS2020.reduce((totalPrice, order) => {
		const price = PRODUCTS.find(
			(product) => product.id === order.idProduct
		).price;
		return (totalPrice += price * order.count);
	}, 0);

	// Сумма заказов в`ORDERS2025`
	const totalPriceORDERS2025 = ORDERS2025.reduce((totalPrice, order) => {
		const price = PRODUCTS.find(
			(product) => product.id === order.idProduct
		).price;
		return (totalPrice += price * order.count);
	}, 0);

	// Получить отсортированный список пользователей по количеству заказов
	function getUsers(array) {
		const usersResult = [];

		array.forEach((orderItem) => {
			// Найти пользователя в массиве с результатом
			const user = usersResult.find(
				(userItem) => userItem.id == orderItem.idUser
			);

			// Сформировать новый заказ
			const newOrder = { id: orderItem.idProduct, count: orderItem.count };

			// Сформировать нового пользователя
			const newUser = { id: orderItem.idUser, orders: [newOrder] };

			if (user) {
				// Если пользователь есть
				// Запушить новый заказ
				user.orders.push(newOrder);
			} else {
				// Если пользователь есть
				// Запушить нового пользователя
				usersResult.push(newUser);
			}
		});

		return (
			usersResult
				// Вернуть новый массив без лишних данных
				.map((user) => ({ id: user.id, orders: user.orders.length }))
				// После отсортировать его по количеству заказов
				.sort((a, b) => b.orders - a.orders)
		);
	}

	// Получить отсортированный список продуктов по количеству заказов
	function getProduct(array) {
		const productResult = [];

		array.forEach((orderItem) => {
			// Найти продукт в массиве с результатом
			const product = productResult.find(
				(userItem) => userItem.id == orderItem.idProduct
			);

			if (product) {
				// Если продукт есть
				// Суммировать их количество
				product.count = product.count + orderItem.count;
			} else {
				// Если продукта нет
				// Пушить новый продукт к результату
				productResult.push({ id: orderItem.idProduct, count: orderItem.count });
			}
		});

		// Вернуть отсортированный результат по количеству проданных продуктов
		return productResult.sort((a, b) => b.count - a.count);
	}

	// Объеденный и отсортированный список пользователей по количеству заказов
	const usersList = getUsers([...ORDERS2020, ...ORDERS2025]);

	// Объеденный и отсортированный список продуктов по количеству заказов
	const productList = getProduct([...ORDERS2020, ...ORDERS2025]);

	// Формирование результата
	report.totalSold = totalSoldORDERS2020 + totalSoldORDERS2025;
	report.totalPrice = totalPriceORDERS2020 + totalPriceORDERS2025;
	report.topProducts = productList.slice(0, 10);
	report.topUsers = usersList.slice(0, 10);

	return report;
};

console.log(realizationTask_7_1());
