import { ORDERS2020 } from '../../../data/orders2020.js';
import { ORDERS2025 } from '../../../data/orders2025.js';
import { PRODUCTS } from '../../../data/products.js';

export const realizationTask_7_2 = () => {
	const report = {
		totalSold: 0,
		totalPrice: 0,
		topProducts: [],
		topUsers: [],
	};

	// объединение данных
	const data = [...ORDERS2020, ...ORDERS2025];

	data.forEach((orderElement, index) => {
		// Найти продукт в отчете
		const productData = PRODUCTS.find(
			(productItem) => productItem.id === orderElement.idProduct
		);

		// Найти продукт в отчете
		const productReport = report.topProducts.find(
			(productItem) => productItem.id === orderElement.idProduct
		);

		// Найти пользователя в отчете
		const userReport = report.topUsers.find(
			(userItem) => userItem.id === orderElement.idUser
		);

		// Инициализация нового продукта
		const newProduct = {
			id: orderElement.idProduct,
			count: orderElement.count,
		};

		// Инициализация нового пользователя
		const newUser = { id: orderElement.idUser, orders: 1 };

		// Формирование отчета
		report.totalSold += orderElement.count;
		report.totalPrice += orderElement.count * productData.price;

		// Продукты
		if (productReport) {
			productReport.count += orderElement.count;
		} else {
			report.topProducts.push(newProduct);
		}

		// Пользователи
		if (userReport) {
			userReport.orders += 1;
		} else {
			report.topUsers.push(newUser);
		}

		// Сортировка и обрезка на последний итерации
		if (index === data.length - 1) {
			report.topProducts = report.topProducts
				.sort((a, b) => b.count - a.count)
				.splice(0, 10);

			report.topUsers = report.topUsers
				.sort((a, b) => b.orders - a.orders)
				.splice(0, 10);
		}
	});

	return report;
};

console.log(realizationTask_7_2());
