import { ORDERS2020 } from '../../../data/orders2020.js';
import { ORDERS2025 } from '../../../data/orders2025.js';
import { PRODUCTS } from '../../../data/products.js';
import { USERS } from '../../../data/users.js';

// Без объединенного количества

export const realizationTask_10_1 = () => {
	const usersResult = [];

	// Изолируем в функции логику для дальнейшего переиспользования
	function handlerData(array) {
		array.forEach((order) => {
			// Найти пользователя в результате
			const user = usersResult.find((user) => user.id === order.idUser);

			if (user) {
				// Если пользователь найден
				// создаем новый продукт
				const newProduct = {
					id: order.idProduct,
					count: order.count,
				};

				//  И пушим его
				user.orders.push(newProduct);
			} else {
				// Если пользователь не был найден
				// Создаем  продукт
				const newProduct = {
					id: order.idProduct,
					count: order.count,
				};

				// Создаем пользователя
				const newUser = {
					id: order.idUser,
					orders: [newProduct],
				};

				// Пушим пользователя в результат
				usersResult.push(newUser);
			}
		});
	}

	// Объединить данные из массивов
	handlerData(ORDERS2020);
	handlerData(ORDERS2025);

	return usersResult;
};

console.log(realizationTask_10_1());
