import { ORDERS2020 } from '../../../data/orders2020.js';
import { ORDERS2025 } from '../../../data/orders2025.js';
import { PRODUCTS } from '../../../data/products.js';
import { USERS } from '../../../data/users.js';

export const realizationTask_10_2 = () => {
	const usersResult = [];

	// Изолируем в функции логику для дальнейшего переиспользования
	function handlerData(array) {
		array.forEach((order) => {
			// Найти пользователя в результате
			const user = usersResult.find((user) => user.id === order.idUser);

			if (user) {
				// Если пользователь найден
				// То ищем у него продукт с таким же id
				const product = user.orders.find(
					(product) => product.id === order.idProduct
				);

				const newProduct = {
					id: order.idProduct,
					count: order.count,
				};

				if (product) {
					// Если продукт был найден
					// То объединяем их количество
					product.count = product.count + order.count;
				} else {
					// Если продукт не был найден
					// То пушем его в заказы
					user.orders.push(newProduct);
				}
			} else {
				// Если пользователь не был найден
				// Создаем  продукт
				const newProduct = {
					id: order.idProduct,
					count: order.count,
				};

				// Создаем пользователя
				const newUser = {
					id: order.idUser,
					orders: [newProduct],
				};

				// Пушим пользователя в результат
				usersResult.push(newUser);
			}
		});
	}

	// Объединить данные из массивов
	handlerData(ORDERS2020);
	handlerData(ORDERS2025);

	return usersResult;
};

console.log(realizationTask_10_2());
