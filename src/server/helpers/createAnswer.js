import { validationAnswer } from './validation/validationAnswer.js';

const defaultAnswer = {
	ok: null,
	status: null,
	code: null,
	data: null,
	error: null,
	throttledMs: null,
};

export const createAnswer = (data) => {
	const answer = { ...defaultAnswer, ...data };
	const isValidAnswer = validationAnswer(answer);

	if (isValidAnswer.status) {
		return answer;
	} else {
		throw new Error(JSON.stringify(isValidAnswer.problems.join(', ')));
	}
};
