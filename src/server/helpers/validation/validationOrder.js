import { validationIdProduct } from './validationIdProduct.js';
import { validationIdUser } from './validationIdUser.js';
import { validationNumber } from './validationNumber.js';

export const validationOrder = (order) => {
	const result = { status: true, problems: [] };

	const idUserResult = validationIdUser(order.idUser);
	if (!idUserResult.status) {
		result.problems.push(...idUserResult.problems);
	}

	const idProductResult = validationIdProduct(order.idProduct);
	if (!idProductResult.status) {
		result.problems.push(...idProductResult.problems);
	}

	const countResult = validationNumber(order.count, 'count');
	if (!countResult.status) {
		result.problems.push(...countResult.problems);
	}

	if (result.problems.length) {
		result.status = false;
	}

	return result;
};
