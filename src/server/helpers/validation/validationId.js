export const validationId = (id) => {
	const result = {
		status: true,
		problems: [],
	};

	if (!id) {
		result.problems.push('ID не может быть пустым');
	}

	if (typeof id !== 'string') {
		result.problems.push('ID должен быть строкой');
	}

	if (id && id.length < 6) {
		result.problems.push('ID не может быть меньше 5-ти символов');
	}

	if (result.problems.length) {
		result.status = false;
	}

	return result;
};
