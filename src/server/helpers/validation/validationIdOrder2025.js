import { validationId } from './validationId.js';

export const validationIdOrder2025 = (id) => {
	const result = {
		status: true,
		problems: [],
	};

	const idResult = validationId(id);

	if (!idResult.status) {
		result.problems.push(...idResult.problems);
	}

	if (id && !id.includes('o2025_')) {
		result.problems.push('ID заказа должно начинаться с "o2025_"');
	}

	if (result.problems.length) {
		result.status = false;
	}

	return result;
};
