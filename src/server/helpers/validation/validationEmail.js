export const validationEmail = (email) => {
	const result = { status: true, problems: [] };

	if (!email) {
		result.problems.push('Email не может быть пустым');
	}

	if (typeof email !== 'string') {
		result.problems.push('Email пользователя должен быть строкой');
	}

	if (email && !email.includes('@')) {
		result.problems.push('Email должен содержать @');
	}

	if (email && email.length < 6) {
		result.problems.push('Email не может быть меньше 6-ти символов');
	}

	if (result.problems.length) {
		result.status = false;
	}

	return result;
};
