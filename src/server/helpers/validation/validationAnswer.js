import { validationNumber } from './validationNumber.js';
import { validationString } from './validationString.js';

export const validationAnswer = (answer) => {
	const result = {
		status: true,
		problems: [],
	};

	if (!answer) {
		result.problems.push('Ответ не может быть пустым');
	}

	// Ok
	if (answer.ok !== true && answer.ok !== false) {
		result.problems.push('"ok" должен быть true или false');
	}

	// Status
	const statusResult = validationString(answer.status, 'status');
	if (!statusResult.status) {
		result.problems.push(...statusResult.problems);
	}

	const codeResult = validationNumber(answer.code, 'code');
	if (!codeResult.status) {
		result.problems.push(...codeResult.problems);
	}

	const throttledMsResult = validationNumber(answer.throttledMs, 'throttledMs');
	if (!throttledMsResult.status) {
		result.problems.push(...throttledMsResult.problems);
	}

	if (answer.data && answer.error) {
		result.problems.push(
			'Не может одновременно быть данных("data") и ошибки("error")'
		);
	}

	if (!answer.data && !answer.error) {
		result.problems.push(
			'Не может одновременно не быть, не данных("data") не ошибок("error")'
		);
	}

	if (result.problems.length) {
		result.status = false;
	}

	return result;
};
