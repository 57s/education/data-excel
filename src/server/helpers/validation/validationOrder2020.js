import { validationIdOrder2020 } from './validationIdOrder2020.js';
import { validationOrder } from './validationOrder.js';

export const validationOrder2020 = (order) => {
	const result = { status: true, problems: [] };

	const idResult = validationIdOrder2020(order.id);
	if (!idResult.status) {
		result.problems.push(...idResult.problems);
	}

	const baseOrderResult = validationOrder(order);
	if (!baseOrderResult.status) {
		result.problems.push(...baseOrderResult.problems);
	}

	if (result.problems.length) {
		result.status = false;
	}

	return result;
};
