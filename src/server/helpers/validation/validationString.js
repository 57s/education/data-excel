export const validationString = (prop, name = 'имя') => {
	const result = { status: true, problems: [] };

	if (!prop) {
		result.problems.push(`Свойство "${name}" не может быть пустым`);
	}

	if (typeof prop !== 'string') {
		result.problems.push(`Свойство "${name}" пользователя должно быть строкой`);
	}

	if (prop && prop.length < 2) {
		result.problems.push(
			`'Свойство "${name}" не может быть меньше 2-х символов'`
		);
	}

	if (result.problems.length) {
		result.status = false;
	}

	return result;
};
