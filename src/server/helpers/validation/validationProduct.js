import { validationIdProduct } from './validationIdProduct.js';
import { validationNumber } from './validationNumber.js';
import { validationString } from './validationString.js';

export const validationProduct = (product) => {
	const result = { status: true, problems: [] };

	const idResult = validationIdProduct(product.id);
	if (!idResult.status) {
		result.problems.push(...idResult.problems);
	}

	const nameResult = validationString(product.name, 'name');
	if (!nameResult.status) {
		result.problems.push(...nameResult.problems);
	}

	const typeResult = validationString(product.type, 'type');
	if (!typeResult.status) {
		result.problems.push(...typeResult.problems);
	}

	const priceResult = validationNumber(product.price, 'price');
	if (!priceResult.status) {
		result.problems.push(...priceResult.problems);
	}

	if (result.problems.length) {
		result.status = false;
	}

	return result;
};
