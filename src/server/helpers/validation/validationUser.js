import { validationEmail } from './validationEmail.js';
import { validationIdUser } from './validationIdUser.js';
import { validationString } from './validationString.js';

export const validationUser = (user) => {
	const result = { status: true, problems: [] };

	const idResult = validationIdUser(user.id);
	if (!idResult.status) {
		result.problems.push(...idResult.problems);
	}

	const nameResult = validationString(user.name, 'name');
	if (!nameResult.status) {
		result.problems.push(...nameResult.problems);
	}

	const emailResult = validationEmail(user.email);
	if (!emailResult.status) {
		result.problems.push(...emailResult.problems);
	}

	if (result.problems.length) {
		result.status = false;
	}

	return result;
};
