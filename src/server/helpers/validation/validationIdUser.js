import { validationId } from './validationId.js';

export const validationIdUser = (id) => {
	const result = {
		status: true,
		problems: [],
	};

	const idResult = validationId(id);

	if (!idResult.status) {
		result.problems.push(...idResult.problems);
	}

	if (id && id[0] !== 'u') {
		result.problems.push('ID пользователя должно начинаться с "u"');
	}

	if (result.problems.length) {
		result.status = false;
	}

	return result;
};
