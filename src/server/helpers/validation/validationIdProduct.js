import { validationId } from './validationId.js';

export const validationIdProduct = (id) => {
	const result = {
		status: true,
		problems: [],
	};

	const idResult = validationId(id);

	if (!idResult.status) {
		result.problems.push(...idResult.problems);
	}

	if (id && id[0] !== 'p') {
		result.problems.push('ID продукта должно начинаться с "p"');
	}

	if (result.problems.length) {
		result.status = false;
	}

	return result;
};
