export const validationNumber = (prop, name = 'имя') => {
	const result = { status: true, problems: [] };

	if (typeof prop !== 'number') {
		result.problems.push(`Свойство "${name}" должно быть числом`);
	}

	if (result.problems.length) {
		result.status = false;
	}

	return result;
};
