export const SERVER_CODES = {
	ok: {
		code: 200,
		message: 'OK',
	},

	notAvailable: {
		code: 503,
		message: 'Not available',
	},

	notFound: {
		code: 404,
		message: 'Not Found',
	},

	badRequest: {
		code: 400,
		message: 'Bad request',
	},

	created: {
		code: 201,
		message: 'Created',
	},

	deleted: {
		code: 204,
		message: 'Deleted',
	},

	updated: {
		code: 204,
		message: 'Updated',
	},
};
