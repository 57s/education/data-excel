export const DB_PATH = {
	users: './src/server/files/users.json',
	usersId: './src/server/files/users-id.json',
	products: './src/server/files/products.json',
	productsId: './src/server/files/products-id.json',
	products_rest: './src/server/files/products_rest.json',
	orders2020: './src/server/files/orders2020.json',
	orders2020Id: './src/server/files/orders2020-id.json',
	orders2025: './src/server/files/orders2025.json',
	orders2025Id: './src/server/files/orders2025-id.json',
};
