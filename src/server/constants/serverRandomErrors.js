export const SERVER_RANDOM_ERRORS = [
	{
		message: 'Сервер плохо себя чувствует',
		problems: ['Сервер болеет'],
	},
	{
		message: 'Сервер устал и ничего не вернул',
		problems: ['Сервер спит'],
	},
	{
		message: 'Сервер пьян',
		problems: ['Парам пам пам'],
	},
	{
		message: 'Сервер пал смертью храбрых',
		problems: ['🫡'],
	},
];
