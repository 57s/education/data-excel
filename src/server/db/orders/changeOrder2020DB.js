import { DB_PATH } from '../../constants/path.js';
import { writeFileJson } from '../../../utils/file.js';
import { SERVER_CODES } from '../../constants/serverCodes.js';
import { createAnswer } from '../../helpers/createAnswer.js';
import { getOrdersAll2020DB } from './getOrdersAll2020DB.js';
import { validationOrder2020 } from '../../helpers/validation/validationOrder2020.js';

export const changeOrder2020DB = (id, changes) => {
	const startTime = performance.now();
	return new Promise(async (resolve, reject) => {
		if (changes.id) {
			const endTime = performance.now();
			const error = {
				message: 'Ошибка валидации',
				problems: ['Нельзя менять id'],
			};
			const answer = createAnswer({
				ok: false,
				error,
				status: SERVER_CODES.badRequest.message,
				code: SERVER_CODES.badRequest.code,
				throttledMs: endTime - startTime,
			});

			reject(answer);
		}

		const orders = await getOrdersAll2020DB();
		const currentOrder = orders.data.find((order) => order.id === id);
		if (!currentOrder) {
			const endTime = performance.now();
			const error = {
				message: 'Нет такого заказа',
				problems: ['Заказ не был найден'],
			};
			const answer = createAnswer({
				ok: false,
				error,
				status: SERVER_CODES.notFound.message,
				code: SERVER_CODES.notFound.code,
				throttledMs: endTime - startTime,
			});

			reject(answer);
		}

		const updateOrder = {
			...currentOrder,
			...changes,
		};

		const isValid = validationOrder2020(updateOrder);

		if (isValid.status) {
			const newOrders = orders.data.map((order) =>
				order.id === id ? updateOrder : order
			);

			await writeFileJson(DB_PATH.orders2020, newOrders);
			const endTime = performance.now();

			const answer = createAnswer({
				ok: true,
				data: updateOrder,
				status: SERVER_CODES.updated.message,
				code: SERVER_CODES.updated.code,
				throttledMs: endTime - startTime,
			});

			resolve(answer);
		} else {
			const endTime = performance.now();
			const error = {
				message: 'Ошибка валидации',
				problems: isValid.problems,
			};
			const answer = createAnswer({
				ok: false,
				error,
				status: SERVER_CODES.badRequest.message,
				code: SERVER_CODES.badRequest.code,
				throttledMs: endTime - startTime,
			});

			reject(answer);
		}
	});
};

// const orderDataUpdate = {
// 	count: 9,
// };

// changeOrder2020DB('o2020_9999', orderDataUpdate)
// 	.then(console.log)
// 	.catch(console.error);
