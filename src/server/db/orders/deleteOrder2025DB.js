import { DB_PATH } from '../../constants/path.js';
import { writeFileJson } from '../../../utils/file.js';
import { SERVER_CODES } from '../../constants/serverCodes.js';
import { createAnswer } from '../../helpers/createAnswer.js';
import { getOrdersAll2025DB } from './getOrdersAll2025DB.js';

export const deleteOrder2025 = (id) => {
	const startTime = performance.now();
	return new Promise(async (resolve, reject) => {
		const orders = await getOrdersAll2025DB();
		const deletedOrder = orders.data.find((order) => order.id === id);

		if (deletedOrder) {
			const newOrders = orders.data.filter((order) => order.id !== id);
			await writeFileJson(DB_PATH.orders2025, newOrders);

			const endTime = performance.now();
			const answer = createAnswer({
				ok: true,
				data: deletedOrder,
				status: SERVER_CODES.deleted.message,
				code: SERVER_CODES.deleted.code,
				throttledMs: endTime - startTime,
			});

			resolve(answer);
		} else {
			const endTime = performance.now();
			const error = {
				message: 'Нет такого заказа',
				problems: 'Заказ не был найден',
			};
			const answer = createAnswer({
				ok: false,
				error,
				status: SERVER_CODES.notFound.message,
				code: SERVER_CODES.notFound.code,
				throttledMs: endTime - startTime,
			});

			reject(answer);
		}
	});
};

// deleteOrder2025('o2025_00048').then(console.log).catch(console.error);
