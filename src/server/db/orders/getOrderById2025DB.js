import { SERVER_CODES } from '../../constants/serverCodes.js';
import { createAnswer } from '../../helpers/createAnswer.js';
import { getOrdersAll2025DB } from './getOrdersAll2025DB.js';

export const getOrderById2025DB = (id) => {
	const startTime = performance.now();
	return new Promise(async (resolve, reject) => {
		try {
			const orders = await getOrdersAll2025DB();
			const currentOrder = orders.data.find((order) => order.id === id);
			if (currentOrder) {
				const endTime = performance.now();
				const answer = createAnswer({
					ok: true,
					data: currentOrder,
					status: SERVER_CODES.ok.message,
					code: SERVER_CODES.ok.code,
					throttledMs: endTime - startTime,
				});

				resolve(answer);
			} else {
				const endTime = performance.now();
				const error = {
					message: 'Нет такого заказа',
					problems: ['Заказ не был найден'],
				};
				const answer = createAnswer({
					ok: false,
					error,
					status: SERVER_CODES.notFound.message,
					code: SERVER_CODES.notFound.code,
					throttledMs: endTime - startTime,
				});

				reject(answer);
			}
		} catch (error) {
			const endTime = performance.now();
			const answer = createAnswer({
				ok: false,
				error,
				status: SERVER_CODES.badRequest.message,
				code: SERVER_CODES.badRequest.code,
				throttledMs: endTime - startTime,
			});
			reject(answer);
		}
	});
};

// getOrderById2025DB('o_2025_51').then(console.log).catch(console.error);
