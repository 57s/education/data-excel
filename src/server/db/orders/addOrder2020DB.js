import { DB_PATH } from '../../constants/path.js';
import { writeFileJson } from '../../../utils/file.js';
import { createAnswer } from '../../helpers/createAnswer.js';
import { SERVER_CODES } from '../../constants/serverCodes.js';
import { getOrdersAll2020DB } from './getOrdersAll2020DB.js';
import { validationOrder2020 } from '../../helpers/validation/validationOrder2020.js';
import { getIdOrder2020 } from '../../../utils/getId.js';

export const addOrder2020DB = (data) => {
	const startTime = performance.now();
	return new Promise(async (resolve, reject) => {
		const newOrder = { ...data, id: getIdOrder2020() };
		const orders = await getOrdersAll2020DB();
		const isValid = validationOrder2020(newOrder);

		if (isValid.status) {
			orders.data.push(newOrder);
			await writeFileJson(DB_PATH.orders2020, orders.data);
			const endTime = performance.now();

			const answer = createAnswer({
				ok: true,
				data: newOrder,
				status: SERVER_CODES.created.message,
				code: SERVER_CODES.created.code,
				throttledMs: endTime - startTime,
			});

			resolve(answer);
		} else {
			const endTime = performance.now();
			const error = {
				message: 'Ошибка валидации',
				problems: isValid.problems,
			};
			const answer = createAnswer({
				ok: false,
				error,
				status: SERVER_CODES.badRequest.message,
				code: SERVER_CODES.badRequest.code,
				throttledMs: endTime - startTime,
			});

			reject(answer);
		}
	});
};

// const testOrder = {
// 	idUser: 'u00008',
// 	idProduct: 'p00010',
// 	count: 300,
// };

// addOrder2020DB(testOrder).then(console.log).catch(console.error);
