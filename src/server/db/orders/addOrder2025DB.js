import { DB_PATH } from '../../constants/path.js';
import { writeFileJson } from '../../../utils/file.js';
import { createAnswer } from '../../helpers/createAnswer.js';
import { SERVER_CODES } from '../../constants/serverCodes.js';
import { getIdOrder2025 } from '../../../utils/getId.js';
import { getOrdersAll2025DB } from './getOrdersAll2025DB.js';
import { validationOrder2025 } from '../../helpers/validation/validationOrder2025.js';

export const addOrder2025DB = (data) => {
	const startTime = performance.now();
	return new Promise(async (resolve, reject) => {
		const newOrder = { ...data, id: getIdOrder2025() };
		const orders = await getOrdersAll2025DB();
		const isValid = validationOrder2025(newOrder);

		if (isValid.status) {
			orders.data.push(newOrder);
			await writeFileJson(DB_PATH.orders2025, orders.data);
			const endTime = performance.now();

			const answer = createAnswer({
				ok: true,
				data: newOrder,
				status: SERVER_CODES.created.message,
				code: SERVER_CODES.created.code,
				throttledMs: endTime - startTime,
			});

			resolve(answer);
		} else {
			const endTime = performance.now();
			const error = {
				message: 'Ошибка валидации',
				problems: isValid.problems,
			};
			const answer = createAnswer({
				ok: false,
				error,
				status: SERVER_CODES.badRequest.message,
				code: SERVER_CODES.badRequest.code,
				throttledMs: endTime - startTime,
			});

			reject(answer);
		}
	});
};

// const testOrder = {
// 	idUser: 'u00008',
// 	idProduct: 'p00010',
// 	count: 2525,
// };

// addOrder2025DB(testOrder).then(console.log).catch(console.error);
