import { DB_PATH } from '../../constants/path.js';
import { writeFileJson } from '../../../utils/file.js';
import { SERVER_CODES } from '../../constants/serverCodes.js';
import { createAnswer } from '../../helpers/createAnswer.js';
import { getOrdersAll2020DB } from './getOrdersAll2020DB.js';

export const deleteOrder2020 = (id) => {
	const startTime = performance.now();
	return new Promise(async (resolve, reject) => {
		const orders = await getOrdersAll2020DB();
		const deletedOrder = orders.data.find((order) => order.id === id);

		if (deletedOrder) {
			const newOrders = orders.data.filter((order) => order.id !== id);
			await writeFileJson(DB_PATH.orders2020, newOrders);

			const endTime = performance.now();
			const answer = createAnswer({
				ok: true,
				data: deletedOrder,
				status: SERVER_CODES.deleted.message,
				code: SERVER_CODES.deleted.code,
				throttledMs: endTime - startTime,
			});

			resolve(answer);
		} else {
			const endTime = performance.now();
			const error = {
				message: 'Нет такого заказа',
				problems: 'Заказ не был найден',
			};
			const answer = createAnswer({
				ok: false,
				error,
				status: SERVER_CODES.notFound.message,
				code: SERVER_CODES.notFound.code,
				throttledMs: endTime - startTime,
			});

			reject(answer);
		}
	});
};

// deleteOrder2020('o2020_00050').then(console.log).catch(console.error);
