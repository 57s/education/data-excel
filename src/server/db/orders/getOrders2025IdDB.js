import { getFileJson } from '../../../utils/file.js';
import { ORDERS2025 } from '../../../data/orders2025.js';

import { DB_PATH } from '../../constants/path.js';
import { createAnswer } from '../../helpers/createAnswer.js';
import { SERVER_CODES } from '../../constants/serverCodes.js';

export const getOrders2025IdDB = () => {
	const startTime = performance.now();
	return new Promise(async (resolve, reject) => {
		const ordersId = ORDERS2025.map((order) => order.id);
		try {
			const orders = await getFileJson(DB_PATH.orders2025Id, ordersId);
			const endTime = performance.now();
			const answer = createAnswer({
				ok: true,
				data: orders,
				status: SERVER_CODES.ok.message,
				code: SERVER_CODES.ok.code,
				throttledMs: endTime - startTime,
			});

			resolve(answer);
		} catch (error) {
			const endTime = performance.now();
			const answer = createAnswer({
				ok: false,
				error,
				status: SERVER_CODES.badRequest.message,
				code: SERVER_CODES.badRequest.code,
				throttledMs: endTime - startTime,
			});
			reject(answer);
		}
	});
};

// getOrders2025IdDB().then(console.log).catch(console.error);
