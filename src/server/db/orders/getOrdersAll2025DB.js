import { ORDERS2025 } from '../../../data/orders2025.js';
import { getFileJson } from '../../../utils/file.js';
import { DB_PATH } from '../../constants/path.js';
import { SERVER_CODES } from '../../constants/serverCodes.js';
import { createAnswer } from '../../helpers/createAnswer.js';

export const getOrdersAll2025DB = () => {
	const startTime = performance.now();
	return new Promise(async (resolve, reject) => {
		try {
			const orders = await getFileJson(DB_PATH.orders2025, ORDERS2025);
			const endTime = performance.now();
			const answer = createAnswer({
				ok: true,
				data: orders,
				status: SERVER_CODES.ok.message,
				code: SERVER_CODES.ok.code,
				throttledMs: endTime - startTime,
			});

			resolve(answer);
		} catch (error) {
			const endTime = performance.now();
			const answer = createAnswer({
				ok: false,
				error,
				status: SERVER_CODES.badRequest.message,
				code: SERVER_CODES.badRequest.code,
				throttledMs: endTime - startTime,
			});
			reject(answer);
		}
	});
};

// getOrdersAll2025DB().then(console.log);
