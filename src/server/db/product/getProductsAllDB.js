import { PRODUCTS } from '../../../data/products.js';
import { getFileJson } from '../../../utils/file.js';
import { DB_PATH } from '../../constants/path.js';
import { SERVER_CODES } from '../../constants/serverCodes.js';
import { createAnswer } from '../../helpers/createAnswer.js';

export const getProductsAllDB = () => {
	const startTime = performance.now();
	return new Promise(async (resolve, reject) => {
		try {
			const products = await getFileJson(DB_PATH.products, PRODUCTS);
			const endTime = performance.now();
			const answer = createAnswer({
				ok: true,
				data: products,
				status: SERVER_CODES.ok.message,
				code: SERVER_CODES.ok.code,
				throttledMs: endTime - startTime,
			});

			resolve(answer);
		} catch (error) {
			const endTime = performance.now();
			const answer = createAnswer({
				ok: false,
				error,
				status: SERVER_CODES.badRequest.message,
				code: SERVER_CODES.badRequest.code,
				throttledMs: endTime - startTime,
			});
			reject(answer);
		}
	});
};

// getProductsAllDB().then(console.log).catch(console.error);
