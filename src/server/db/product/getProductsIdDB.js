import { PRODUCTS } from '../../../data/products.js';
import { getFileJson } from '../../../utils/file.js';
import { DB_PATH } from '../../constants/path.js';
import { SERVER_CODES } from '../../constants/serverCodes.js';
import { createAnswer } from '../../helpers/createAnswer.js';

export const getProductsIdDB = () => {
	const startTime = performance.now();
	return new Promise(async (resolve, reject) => {
		const productsId = PRODUCTS.map((product) => product.id);
		try {
			const products = await getFileJson(DB_PATH.productsId, productsId);
			const endTime = performance.now();
			const answer = createAnswer({
				ok: true,
				data: products,
				status: SERVER_CODES.ok.message,
				code: SERVER_CODES.ok.code,
				throttledMs: endTime - startTime,
			});

			resolve(answer);
		} catch (error) {
			const endTime = performance.now();
			const answer = createAnswer({
				ok: false,
				error,
				status: SERVER_CODES.badRequest.message,
				code: SERVER_CODES.badRequest.code,
				throttledMs: endTime - startTime,
			});
			reject(answer);
		}
	});
};

// getProductsIdDB().then(console.log).catch(console.error);
