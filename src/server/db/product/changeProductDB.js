import { DB_PATH } from '../../constants/path.js';
import { writeFileJson } from '../../../utils/file.js';
import { SERVER_CODES } from '../../constants/serverCodes.js';
import { createAnswer } from '../../helpers/createAnswer.js';
import { getProductsAllDB } from './getProductsAllDB.js';
import { validationProduct } from '../../helpers/validation/validationProduct.js';

export const changeProductDB = (id, changes) => {
	const startTime = performance.now();
	return new Promise(async (resolve, reject) => {
		if (changes.id) {
			const endTime = performance.now();
			const error = {
				message: 'Ошибка валидации',
				problems: ['Нельзя менять id'],
			};
			const answer = createAnswer({
				ok: false,
				error,
				status: SERVER_CODES.badRequest.message,
				code: SERVER_CODES.badRequest.code,
				throttledMs: endTime - startTime,
			});

			reject(answer);
		}

		const products = await getProductsAllDB();
		const currentProduct = products.data.find((product) => product.id === id);
		if (!currentProduct) {
			const endTime = performance.now();
			const error = {
				message: 'Нет такого продукта',
				problems: ['Продукт не был найден'],
			};
			const answer = createAnswer({
				ok: false,
				error,
				status: SERVER_CODES.notFound.message,
				code: SERVER_CODES.notFound.code,
				throttledMs: endTime - startTime,
			});

			reject(answer);
		}
		const updateProduct = {
			...currentProduct,
			...changes,
		};

		console.log(updateProduct);

		const isValid = validationProduct(updateProduct);

		if (isValid.status) {
			const newProducts = products.data.map((product) =>
				product.id === id ? updateProduct : product
			);

			await writeFileJson(DB_PATH.products, newProducts);
			const endTime = performance.now();

			const answer = createAnswer({
				ok: true,
				data: updateProduct,
				status: SERVER_CODES.updated.message,
				code: SERVER_CODES.updated.code,
				throttledMs: endTime - startTime,
			});

			resolve(answer);
		} else {
			const endTime = performance.now();
			const error = {
				message: 'Ошибка валидации',
				problems: isValid.problems,
			};
			const answer = createAnswer({
				ok: false,
				error,
				status: SERVER_CODES.badRequest.message,
				code: SERVER_CODES.badRequest.code,
				throttledMs: endTime - startTime,
			});

			reject(answer);
		}
	});
};

// const productDataUpdate = {
// 	name: 'test product update 2',
// 	price: 500,
// };

// changeProductDB('product-2', productDataUpdate)
// 	.then(console.log)
// 	.catch(console.error);
