import { DB_PATH } from '../../constants/path.js';
import { writeFileJson } from '../../../utils/file.js';
import { SERVER_CODES } from '../../constants/serverCodes.js';
import { createAnswer } from '../../helpers/createAnswer.js';
import { getProductsAllDB } from './getProductsAllDB.js';

export const deleteProductDB = (id) => {
	const startTime = performance.now();
	return new Promise(async (resolve, reject) => {
		const products = await getProductsAllDB();
		const deletedProduct = products.data.find((product) => product.id === id);

		if (deletedProduct) {
			const newProducts = products.data.filter((product) => product.id !== id);
			await writeFileJson(DB_PATH.products, newProducts);

			const endTime = performance.now();
			const answer = createAnswer({
				ok: true,
				data: deletedProduct,
				status: SERVER_CODES.deleted.message,
				code: SERVER_CODES.deleted.code,
				throttledMs: endTime - startTime,
			});

			resolve(answer);
		} else {
			const endTime = performance.now();
			const error = {
				status: SERVER_CODES.notFound.message,
				code: SERVER_CODES.notFound.code,
			};
			const answer = createAnswer({
				ok: false,
				error,
				status: SERVER_CODES.badRequest.message,
				code: SERVER_CODES.badRequest.code,
				throttledMs: endTime - startTime,
			});

			reject(answer);
		}
	});
};

// deleteProductDB('product-2').then(console.log).catch(console.error);
