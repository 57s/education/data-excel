import { SERVER_CODES } from '../../constants/serverCodes.js';
import { createAnswer } from '../../helpers/createAnswer.js';
import { getProductsRestAllDB } from './getProductsRestAllDB.js';

export const getProductsRestByIdDB = (id) => {
	const startTime = performance.now();
	return new Promise(async (resolve, reject) => {
		try {
			const productsRest = await getProductsRestAllDB();
			const currentProductRest = productsRest.data.find(
				(product) => product.id === id
			);

			if (currentProductRest) {
				const endTime = performance.now();
				const answer = createAnswer({
					ok: true,
					data: currentProductRest,
					status: SERVER_CODES.ok.message,
					code: SERVER_CODES.ok.code,
					throttledMs: endTime - startTime,
				});

				resolve(answer);
			} else {
				const endTime = performance.now();
				const error = {
					message: 'Нет такого продукта',
					problems: ['Продукт не был найден'],
				};
				const answer = createAnswer({
					ok: false,
					error,
					status: SERVER_CODES.notFound.message,
					code: SERVER_CODES.notFound.code,
					throttledMs: endTime - startTime,
				});

				reject(answer);
			}
		} catch (error) {
			const endTime = performance.now();
			const answer = createAnswer({
				ok: false,
				error,
				status: SERVER_CODES.badRequest.message,
				code: SERVER_CODES.badRequest.code,
				throttledMs: endTime - startTime,
			});
			reject(answer);
		}
	});
};

// getProductsRestByIdDB('p00040').then(console.log).catch(console.error);
