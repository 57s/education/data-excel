import { writeFileJson } from '../../../utils/file.js';
import { getProductsAllDB } from './getProductsAllDB.js';
import { DB_PATH } from '../../constants/path.js';
import { validationProduct } from '../../helpers/validation/validationProduct.js';
import { createAnswer } from '../../helpers/createAnswer.js';
import { SERVER_CODES } from '../../constants/serverCodes.js';
import { getIdProduct } from '../../../utils/getId.js';

export const addProductsDB = (data) => {
	const startTime = performance.now();
	return new Promise(async (resolve, reject) => {
		const newProduct = {
			...data,
			id: getIdProduct(),
		};
		const products = await getProductsAllDB();
		const isValid = validationProduct(newProduct);

		if (isValid.status) {
			products.data.push(newProduct);
			await writeFileJson(DB_PATH.products, products.data);
			const endTime = performance.now();

			const answer = createAnswer({
				ok: true,
				data: newProduct,
				status: SERVER_CODES.created.message,
				code: SERVER_CODES.created.code,
				throttledMs: endTime - startTime,
			});

			resolve(answer);
		} else {
			const endTime = performance.now();
			const error = {
				message: 'Ошибка валидации',
				problems: isValid.problems,
			};
			const answer = createAnswer({
				ok: false,
				error,
				status: SERVER_CODES.badRequest.message,
				code: SERVER_CODES.badRequest.code,
				throttledMs: endTime - startTime,
			});

			reject(answer);
		}
	});
};

// const testProduct = {
// 	type: 'food',
// 	name: 'dried fruits',
// 	price: 190,
// };

// addProductsDB(testProduct).then(console.log).catch(console.error);
