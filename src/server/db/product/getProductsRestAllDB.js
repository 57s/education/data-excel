import { PRODUCTS_REST } from '../../../data/products-rest.js';
import { getFileJson } from '../../../utils/file.js';
import { DB_PATH } from '../../constants/path.js';
import { SERVER_CODES } from '../../constants/serverCodes.js';
import { createAnswer } from '../../helpers/createAnswer.js';

export const getProductsRestAllDB = () => {
	const startTime = performance.now();
	return new Promise(async (resolve, reject) => {
		try {
			const productsRest = await getFileJson(
				DB_PATH.products_rest,
				PRODUCTS_REST
			);
			const endTime = performance.now();
			const answer = createAnswer({
				ok: true,
				data: productsRest,
				status: SERVER_CODES.ok.message,
				code: SERVER_CODES.ok.code,
				throttledMs: endTime - startTime,
			});

			resolve(answer);
		} catch (error) {
			const endTime = performance.now();
			const answer = createAnswer({
				ok: false,
				error,
				status: SERVER_CODES.badRequest.message,
				code: SERVER_CODES.badRequest.code,
				throttledMs: endTime - startTime,
			});
			reject(answer);
		}
	});
};

// getProductsRestAllDB().then(console.log).catch(console.error);
