import { getUserByIdDB } from './users/getUserByIdDB.js';
import { getUsersAllDB } from './users/getUsersAllDB.js';
import { getUsersIdDB } from './users/getUsersIdDB.js';
import { addUserDB } from './users/addUserDB.js';
import { changeUserDB } from './users/changeUserDB.js';
import { deleteUserDB } from './users/deleteUserDB.js';

import { getProductsByIdDB } from './product/getProductsByIdDB.js';
import { getProductsAllDB } from './product/getProductsAllDB.js';
import { getProductsIdDB } from './product/getProductsIdDB.js';
import { getProductsRestAllDB } from './product/getProductsRestAllDB.js';
import { getProductsRestByIdDB } from './product/getProductsRestByIdDB.js';
import { addProductsDB } from './product/addProductsDB.js';
import { changeProductDB } from './product/changeProductDB.js';
import { deleteProductDB } from './product/deleteProductDB.js';

import { getOrderById2020DB } from './orders/getOrderById2020DB.js';
import { getOrdersAll2020DB } from './orders/getOrdersAll2020DB.js';
import { getOrders2020IdDB } from './orders/getOrders2020IdDB.js';
import { addOrder2020DB } from './orders/addOrder2020DB.js';
import { changeOrder2020DB } from './orders/changeOrder2020DB.js';
import { deleteOrder2020 } from './orders/deleteOrder2020DB.js';

import { getOrderById2025DB } from './orders/getOrderById2025DB.js';
import { getOrdersAll2025DB } from './orders/getOrdersAll2025DB.js';
import { getOrders2025IdDB } from './orders/getOrders2025IdDB.js';
import { addOrder2025DB } from './orders/addOrder2025DB.js';
import { changeOrder2025DB } from './orders/changeOrder2025DB.js';
import { deleteOrder2025 } from './orders/deleteOrder2025DB.js';

export const db = {
	getUsersAllDB,
	getUserByIdDB,
	getUsersIdDB,
	addUserDB,
	changeUserDB,
	deleteUserDB,

	getProductsByIdDB,
	getProductsAllDB,
	getProductsIdDB,
	getProductsRestAllDB,
	getProductsRestByIdDB,
	addProductsDB,
	changeProductDB,
	deleteProductDB,

	getOrderById2020DB,
	getOrdersAll2020DB,
	getOrders2020IdDB,
	addOrder2020DB,
	changeOrder2020DB,
	deleteOrder2020,

	getOrderById2025DB,
	getOrdersAll2025DB,
	getOrders2025IdDB,
	addOrder2025DB,
	changeOrder2025DB,
	deleteOrder2025,
};
