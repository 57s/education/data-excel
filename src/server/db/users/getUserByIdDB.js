import { createAnswer } from '../../helpers/createAnswer.js';
import { SERVER_CODES } from '../../constants/serverCodes.js';
import { getUsersAllDB } from './getUsersAllDB.js';

export const getUserByIdDB = (id) => {
	const startTime = performance.now();
	return new Promise(async (resolve, reject) => {
		try {
			const users = await getUsersAllDB();
			const currentUser = users.data.find((user) => user.id === id);

			if (currentUser) {
				const endTime = performance.now();
				const answer = createAnswer({
					ok: true,
					data: currentUser,
					status: SERVER_CODES.ok.message,
					code: SERVER_CODES.ok.code,
					throttledMs: endTime - startTime,
				});

				resolve(answer);
			} else {
				const endTime = performance.now();
				const error = {
					message: 'Нет такого пользователя',
					problems: ['Пользователь не был найден'],
				};
				const answer = createAnswer({
					ok: false,
					error,
					status: SERVER_CODES.notFound.message,
					code: SERVER_CODES.notFound.code,
					throttledMs: endTime - startTime,
				});

				reject(answer);
			}
		} catch (error) {
			const endTime = performance.now();
			const answer = createAnswer({
				ok: false,
				error,
				status: SERVER_CODES.badRequest.message,
				code: SERVER_CODES.badRequest.code,
				throttledMs: endTime - startTime,
			});
			reject(answer);
		}
	});
};

// getUserByIdDB('u000020').then(console.log).catch(console.error);
