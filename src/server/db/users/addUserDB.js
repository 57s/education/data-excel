import { getUsersAllDB } from './getUsersAllDB.js';
import { DB_PATH } from '../../constants/path.js';
import { writeFileJson } from '../../../utils/file.js';
import { validationUser } from '../../helpers/validation/validationUser.js';
import { createAnswer } from '../../helpers/createAnswer.js';
import { SERVER_CODES } from '../../constants/serverCodes.js';
import { getIdUser } from '../../../utils/getId.js';

export const addUserDB = (data) => {
	const startTime = performance.now();
	return new Promise(async (resolve, reject) => {
		const newUser = {
			...data,
			id: getIdUser(),
		};
		const users = await getUsersAllDB();
		const isValid = validationUser(newUser);

		if (isValid.status) {
			users.data.push(newUser);
			await writeFileJson(DB_PATH.users, users.data);
			const endTime = performance.now();

			const answer = createAnswer({
				ok: true,
				data: newUser,
				status: SERVER_CODES.created.message,
				code: SERVER_CODES.created.code,
				throttledMs: endTime - startTime,
			});

			resolve(answer);
		} else {
			const endTime = performance.now();
			const error = {
				message: 'Ошибка валидации',
				problems: isValid.problems,
			};
			const answer = createAnswer({
				ok: false,
				error,
				status: SERVER_CODES.badRequest.message,
				code: SERVER_CODES.badRequest.code,
				throttledMs: endTime - startTime,
			});

			reject(answer);
		}
	});
};

// const testUser = {
// 	name: 'test',
// 	email: 's3V0g@example.com',
// };

// addUserDB(testUser).then(console.log).catch(console.error);
