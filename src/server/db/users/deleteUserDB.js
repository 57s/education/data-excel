import { getUsersAllDB } from './getUsersAllDB.js';
import { DB_PATH } from '../../constants/path.js';
import { writeFileJson } from '../../../utils/file.js';
import { SERVER_CODES } from '../../constants/serverCodes.js';
import { createAnswer } from '../../helpers/createAnswer.js';

export const deleteUserDB = (id) => {
	const startTime = performance.now();
	return new Promise(async (resolve, reject) => {
		const users = await getUsersAllDB();
		const deletedUser = users.data.find((user) => user.id === id);

		if (deletedUser) {
			const newUsers = users.data.filter((user) => user.id !== id);
			await writeFileJson(DB_PATH.users, newUsers);

			const endTime = performance.now();
			const answer = createAnswer({
				ok: true,
				data: deletedUser,
				status: SERVER_CODES.deleted.message,
				code: SERVER_CODES.deleted.code,
				throttledMs: endTime - startTime,
			});

			resolve(answer);
		} else {
			const endTime = performance.now();
			const error = {
				message: 'Нет такого пользователя',
				problems: 'Пользователь не был найден',
			};
			const answer = createAnswer({
				ok: false,
				error,
				status: SERVER_CODES.notFound.message,
				code: SERVER_CODES.notFound.code,
				throttledMs: endTime - startTime,
			});

			reject(answer);
		}
	});
};

// deleteUserDB('u-123456').then(console.log).catch(console.error);
