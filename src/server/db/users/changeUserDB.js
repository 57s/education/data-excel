import { getUsersAllDB } from './getUsersAllDB.js';
import { DB_PATH } from '../../constants/path.js';
import { writeFileJson } from '../../../utils/file.js';
import { SERVER_CODES } from '../../constants/serverCodes.js';
import { validationUser } from '../../helpers/validation/validationUser.js';
import { createAnswer } from '../../helpers/createAnswer.js';

export const changeUserDB = (id, changes) => {
	const startTime = performance.now();
	return new Promise(async (resolve, reject) => {
		if (changes.id) {
			const endTime = performance.now();
			const error = {
				message: 'Ошибка валидации',
				problems: ['Нельзя менять id'],
			};
			const answer = createAnswer({
				ok: false,
				error,
				status: SERVER_CODES.badRequest.message,
				code: SERVER_CODES.badRequest.code,
				throttledMs: endTime - startTime,
			});

			reject(answer);
		}

		const users = await getUsersAllDB();
		const currentUser = users.data.find((user) => user.id === id);
		if (!currentUser) {
			const endTime = performance.now();
			const error = {
				message: 'Нет такого пользователя',
				problems: ['Пользователь не был найден'],
			};
			const answer = createAnswer({
				ok: false,
				error,
				status: SERVER_CODES.notFound.message,
				code: SERVER_CODES.notFound.code,
				throttledMs: endTime - startTime,
			});

			reject(answer);
		}

		const updateUser = {
			...currentUser,
			...changes,
		};

		const isValid = validationUser(updateUser);

		if (isValid.status) {
			const newUsers = users.data.map((user) =>
				user.id === id ? updateUser : user
			);

			await writeFileJson(DB_PATH.users, newUsers);
			const endTime = performance.now();

			const answer = createAnswer({
				ok: true,
				data: updateUser,
				status: SERVER_CODES.updated.message,
				code: SERVER_CODES.updated.code,
				throttledMs: endTime - startTime,
			});

			resolve(answer);
		} else {
			const endTime = performance.now();
			const error = {
				message: 'Ошибка валидации',
				problems: isValid.problems,
			};
			const answer = createAnswer({
				ok: false,
				error,
				status: SERVER_CODES.badRequest.message,
				code: SERVER_CODES.badRequest.code,
				throttledMs: endTime - startTime,
			});

			reject(answer);
		}
	});
};

// const userDataUpdate = {
// 	name: 'test user update 222',

// };

// changeUserDB('u00002', userDataUpdate).then(console.log).catch(console.error);
