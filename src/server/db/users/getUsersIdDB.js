import { getFileJson } from '../../../utils/file.js';
import { USERS } from '../../../data/users.js';

import { DB_PATH } from '../../constants/path.js';
import { createAnswer } from '../../helpers/createAnswer.js';
import { SERVER_CODES } from '../../constants/serverCodes.js';

export const getUsersIdDB = () => {
	const startTime = performance.now();
	return new Promise(async (resolve, reject) => {
		const usersId = USERS.map((user) => user.id);
		try {
			const users = await getFileJson(DB_PATH.usersId, usersId);
			const endTime = performance.now();
			const answer = createAnswer({
				ok: true,
				data: users,
				status: SERVER_CODES.ok.message,
				code: SERVER_CODES.ok.code,
				throttledMs: endTime - startTime,
			});

			resolve(answer);
		} catch (error) {
			const endTime = performance.now();
			const answer = createAnswer({
				ok: false,
				error,
				status: SERVER_CODES.badRequest.message,
				code: SERVER_CODES.badRequest.code,
				throttledMs: endTime - startTime,
			});
			reject(answer);
		}
	});
};

// getUsersIdDB().then(console.log).catch(console.error);
