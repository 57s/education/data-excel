import './preRun1.js';

const urlBase = 'https://jsonplaceholder.typicode.com';
const urlUsers = `${urlBase}/users`;
const urlUsers1 = `${urlUsers}/u00002`;

fetch(urlUsers1)
	.then((data) => {
		console.log('SUCCESS');
		console.log(data);
	})
	.catch((error) => {
		console.log('ERROR');
		console.error(error);
	});
