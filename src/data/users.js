import { getIdUser } from '../utils/getId.js';

const defUsers = [
	{
		name: 'Kate',
		email: 'kate@gmail.com',
		age: 16,
		city: 'Moscow',
	},

	{
		name: 'Vitalik',
		email: 'vitalik@gmail.com',
		age: 25,
		city: 'Moscow',
	},

	{
		name: 'Elena',
		email: 'elena@gmail.com',
		age: 18,
		city: 'Saint Petersburg',
	},

	{
		name: 'Alexey',
		email: 'alexey@gmail.com',
		age: 34,
		city: 'Novosibirsk',
	},

	{
		name: 'Maria',
		email: 'maria@gmail.com',
		age: 19,
		city: 'Kazan',
	},

	{
		name: 'Ivan',
		email: 'ivan@gmail.com',
		age: 42,
		city: 'Yekaterinburg',
	},

	{
		name: 'Anna',
		email: 'anna@gmail.com',
		age: 28,
		city: 'Kazan',
	},

	{
		name: 'Sergey',
		email: 'sergey@gmail.com',
		age: 15,
		city: 'Samara',
	},

	{
		name: 'Olga',
		email: 'olga@gmail.com',
		age: 70,
		city: 'Omsk',
	},

	{
		name: 'Dmitry',
		email: 'dmitry@gmail.com',
		age: 14,
		city: 'Rostov-on-Don',
	},
];

export const USERS = defUsers.map((user) => ({ ...user, id: getIdUser() }));
