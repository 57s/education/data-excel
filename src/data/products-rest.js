import { PRODUCTS } from './products.js';
import { getRandomNumber } from '../utils/utils.js';

export const PRODUCTS_REST = PRODUCTS.map((product) => ({
	id: product.id,
	rest: getRandomNumber(),
}));
