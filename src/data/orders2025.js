import { getIdOrder2025 } from '../utils/getId.js';

const defOrders2025 = [
	{ idUser: 'u00008', idProduct: 'p00012', count: 1 },
	{ idUser: 'u00009', idProduct: 'p00012', count: 3 },
	{ idUser: 'u00009', idProduct: 'p00013', count: 3 },
	{ idUser: 'u00001', idProduct: 'p00014', count: 2 },
	{ idUser: 'u00002', idProduct: 'p00015', count: 2 },
	{ idUser: 'u00003', idProduct: 'p00016', count: 1 },
	{ idUser: 'u00004', idProduct: 'p00017', count: 1 },
	{ idUser: 'u00005', idProduct: 'p00018', count: 1 },
	{ idUser: 'u00006', idProduct: 'p00019', count: 3 },
	{ idUser: 'u00007', idProduct: 'p00020', count: 3 },
	{ idUser: 'u00008', idProduct: 'p00021', count: 1 },
	{ idUser: 'u00009', idProduct: 'p00022', count: 1 },
	{ idUser: 'u00010', idProduct: 'p00011', count: 3 },
	{ idUser: 'u00010', idProduct: 'p00012', count: 4 },
	{ idUser: 'u00009', idProduct: 'p00011', count: 3 },
	{ idUser: 'u00008', idProduct: 'p00010', count: 2 },
	{ idUser: 'u00007', idProduct: 'p00009', count: 1 },
	{ idUser: 'u00006', idProduct: 'p00008', count: 5 },
	{ idUser: 'u00005', idProduct: 'p00007', count: 6 },
	{ idUser: 'u00004', idProduct: 'p00006', count: 1 },
	{ idUser: 'u00003', idProduct: 'p00005', count: 7 },
	{ idUser: 'u00002', idProduct: 'p00004', count: 2 },
	{ idUser: 'u00001', idProduct: 'p00003', count: 1 },
	{ idUser: 'u00010', idProduct: 'p00002', count: 2 },
	{ idUser: 'u00010', idProduct: 'p00001', count: 1 },
	{ idUser: 'u00009', idProduct: 'p00023', count: 1 },
	{ idUser: 'u00008', idProduct: 'p00024', count: 1 },
	{ idUser: 'u00001', idProduct: 'p00025', count: 1 },
	{ idUser: 'u00004', idProduct: 'p00026', count: 1 },
	{ idUser: 'u00003', idProduct: 'p00027', count: 1 },
	{ idUser: 'u00002', idProduct: 'p00028', count: 1 },
	{ idUser: 'u00005', idProduct: 'p00029', count: 1 },
	{ idUser: 'u00007', idProduct: 'p00030', count: 1 },
	{ idUser: 'u00006', idProduct: 'p00031', count: 1 },
	{ idUser: 'u00008', idProduct: 'p00032', count: 1 },
	{ idUser: 'u00009', idProduct: 'p00033', count: 1 },
	{ idUser: 'u00010', idProduct: 'p00034', count: 1 },
	{ idUser: 'u00001', idProduct: 'p00035', count: 1 },
	{ idUser: 'u00002', idProduct: 'p00036', count: 1 },
	{ idUser: 'u00003', idProduct: 'p00037', count: 1 },
	{ idUser: 'u00004', idProduct: 'p00038', count: 1 },
	{ idUser: 'u00005', idProduct: 'p00039', count: 1 },
	{ idUser: 'u00006', idProduct: 'p00040', count: 1 },
	{ idUser: 'u00007', idProduct: 'p00012', count: 4 },
	{ idUser: 'u00008', idProduct: 'p00009', count: 2 },
	{ idUser: 'u00001', idProduct: 'p00020', count: 2 },
	{ idUser: 'u00001', idProduct: 'p00001', count: 2 },
];

export const ORDERS2025 = defOrders2025.map((order) => ({
	...order,
	id: getIdOrder2025(),
}));
