import { SERVER_CODES } from '../../server/constants/serverCodes.js';
import { createAnswer } from '../../server/helpers/createAnswer.js';
import { db } from '../../server/db/db.js';
import { parseUrl } from '../../utils/parseUrl.js';
import { regExUsersAll, regExUsersId } from '../../utils/regEx.js';

const getId = (path) => path.split('/').pop();

export const myFetch = async (url, options = {}) => {
	const startTime = performance.now();
	const { pathname } = parseUrl(url);

	if (regExUsersAll(pathname)) {
		return db.getUsersAllDB();
	}

	if (regExUsersId(pathname)) {
		const id = getId(pathname);
		const answer = await db.getUserByIdDB(id);
		return answer;
	}

	return new Promise((resolve, reject) => {
		const endTime = performance.now();
		const error = {
			message: 'Неправильный url',
			problems: [`"${pathname}" не обрабатывается сервером`],
		};
		const answer = createAnswer({
			ok: false,
			error,
			status: SERVER_CODES.notFound.message,
			code: SERVER_CODES.notFound.code,
			throttledMs: endTime - startTime,
		});
		reject(answer);
	});
};
