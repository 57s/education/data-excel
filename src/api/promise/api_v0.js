import { db } from '../../server/db/db.js';
import { getServerSimulation } from '../../utils/getServerSimulation.js';

// example config
// {
// throttledMsMin: 500,
// throttledMsMax: 1500,
// probabilityError: 0,
// }

export const apiPromise_v0 = {
	info: {
		name: 'apiPromise_v0',
		description: 'Api для тестов. Полное без ошибок',
	},

	getUserById: (id) => {
		return getServerSimulation({
			getData: () => db.getUserByIdDB(id),
		});
	},
};
