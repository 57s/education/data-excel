function handleDataUsers(dirtyData) {
	const dirtyUsers = dirtyData.users;
	const dirtyOrders = dirtyData.orders;
	const dirtyProducts = dirtyData.products;
	const cleanUsers = [];

	dirtyUsers.forEach(dirtyUser => {
		// Инициализация дополнительных данных
		const orderData = {
			totalSpent: 0,
			totalProducts: 0,
			totalOrders: 0,
			orders: [],
		};

		// Сбор доп данных
		dirtyOrders.forEach(dirtyOrder => {
			if (dirtyUser.id === dirtyOrder.idUser) {
				const productData = dirtyProducts.find(
					productItem => productItem.id === dirtyOrder.idProduct
				);

				orderData.totalSpent += dirtyOrder.count * productData.price;
				orderData.totalProducts += dirtyOrder.count;
				orderData.totalOrders += 1;
				orderData.orders.push(dirtyOrder.id);
			}
		});

		const newCleanUser = {
			...dirtyUser,
			...orderData,
		};

		cleanUsers.push(newCleanUser);
	});

	return cleanUsers.sort((a, b) => b.totalSpent - a.totalSpent);
}

export default handleDataUsers;
