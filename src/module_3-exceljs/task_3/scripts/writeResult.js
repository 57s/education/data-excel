import ExcelJS from 'exceljs';

import writeReport from './writeReport.js';
import writeUsers from './writeUsers.js';
import writeProduct from './writeProduct.js';
import writeOrders from './writeOrders.js';

function writeResult(data) {
	// Создать книгу
	const reportBook = new ExcelJS.Workbook();

	writeReport({ data: data.report, book: reportBook });
	writeUsers({ data: data.users, book: reportBook });
	writeProduct({ data: data.products, book: reportBook });
	writeOrders({ data: data.orders, book: reportBook });

	// Записать файл с результатом
	reportBook.xlsx.writeFile('./src/module_3-exceljs/task_3/output/report.xlsx');
}

export default writeResult;
