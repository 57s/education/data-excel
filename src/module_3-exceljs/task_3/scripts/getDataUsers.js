import ExcelJS from 'exceljs';

async function getDataUsers() {
	const usersBook = new ExcelJS.Workbook();
	const usersBookFile = await usersBook.xlsx.readFile(
		'./src/module_3-exceljs/task_1/output/users.xlsx'
	);

	const usersSheet = usersBookFile.getWorksheet('users');

	const usersData = [];

	usersSheet.eachRow(row => {
		const id = row.values[1];
		const name = row.values[2];
		const age = row.values[3];
		const city = row.values[4];

		const newUser = {
			id,
			name,
			age,
			city,
		};

		// Фикс имени колонки
		if (newUser.id !== 'Id') {
			usersData.push(newUser);
		}
	});

	return usersData;
}

export default getDataUsers;
