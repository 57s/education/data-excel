function writeProduct({ data, book }) {
	// Перейти на лист
	const productSheet = book.addWorksheet('product');

	// Настроить заголовки
	productSheet.columns = [
		{ header: 'Id', key: 'id', width: 10 },
		{ header: 'Name', key: 'name', width: 25 },
		{ header: 'Type', key: 'type', width: 15 },
		{ header: 'Price', key: 'price', width: 10 },
		{ header: 'Total sold', key: 'totalSold', width: 15 },
		{ header: 'Rest', key: 'rest', width: 7 },
	];

	// Заполнение листа
	data.forEach(product => {
		productSheet.addRow(product);
	});
}

export default writeProduct;
