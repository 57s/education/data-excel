function handleDataOrders(dirtyData) {
	const dirtyOrders = dirtyData.orders;
	const dirtyProducts = dirtyData.products;
	const cleanOrders = [];

	dirtyOrders.forEach(dirtyOrder => {
		const productData = dirtyProducts.find(
			dirtyProduct => dirtyProduct.id === dirtyOrder.idProduct
		);

		const newCleanOrder = {
			...dirtyOrder,
			price: productData.price,
			totalPrice: dirtyOrder.count * productData.price,
		};

		cleanOrders.push(newCleanOrder);
	});

	return cleanOrders.sort((a, b) => b.totalPrice - a.totalPrice);
}

export default handleDataOrders;
