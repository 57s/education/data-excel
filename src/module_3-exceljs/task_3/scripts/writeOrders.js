function writeOrders({ data, book }) {
	// Перейти на лист
	const ordersSheet = book.addWorksheet('orders');

	// Настроить заголовки
	ordersSheet.columns = [
		{ header: 'Id', key: 'id', width: 10 },
		{ header: 'Id user', key: 'idUser', width: 10 },
		{ header: 'id product', key: 'idProduct', width: 10 },
		{ header: 'Count', key: 'count', width: 7 },
		{ header: 'Price', key: 'price', width: 10 },
		{ header: 'TotalPrice', key: 'totalPrice', width: 10 },
	];

	// Заполнение листа
	data.forEach(order => {
		ordersSheet.addRow(order);
	});
}

export default writeOrders;
