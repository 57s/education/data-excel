function writeUsers({ data, book }) {
	// Перейти на лист
	const usersSheet = book.addWorksheet('users');

	// Настроить заголовки
	usersSheet.columns = [
		{ header: 'Id', key: 'id', width: 10 },
		{ header: 'Name', key: 'name', width: 10 },
		{ header: 'Age', key: 'age', width: 5 },
		{ header: 'City', key: 'city', width: 20 },
		{ header: 'Total spent', key: 'totalSpent', width: 20 },
		{ header: 'Total products', key: 'totalProducts', width: 20 },
		{ header: 'Total orders', key: 'totalOrders', width: 20 },
		{ header: 'orders', key: 'orders', width: 150 },
	];

	// Заполнение листа
	data.forEach(user => {
		usersSheet.addRow(user);
	});
}

export default writeUsers;
