function writeReport({ data, book }) {
	// Перейти на лист
	const reportSheet = book.addWorksheet('report');
	// Настроить заголовки
	reportSheet.columns = [
		{ header: 'metrics', key: 'metrics', width: 15 },
		{ header: 'Data', key: 'data', width: 100 },
	];

	// Заполнение книги
	Object.entries(data).forEach(([metrics, data]) => {
		reportSheet.addRow({
			metrics,
			data,
		});
	});
}

export default writeReport;
