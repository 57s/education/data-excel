import ExcelJS from 'exceljs';

async function getDataOrders() {
	const ordersBook = new ExcelJS.Workbook();
	const ordersBookFile = await ordersBook.xlsx.readFile(
		'./src/module_3-exceljs/task_1/output/orders.xlsx'
	);

	const ordersSheet2020 = ordersBookFile.getWorksheet('2020');
	const ordersSheet2025 = ordersBookFile.getWorksheet('2025');

	const ordersData = [];

	function getOrder(row) {
		const id = row.values[1];
		const idUser = row.values[2];
		const idProduct = row.values[3];
		const count = row.values[4];

		const newOrder = {
			id,
			idUser,
			idProduct,
			count,
		};

		// Фикс имени колонки
		if (newOrder.id !== 'Id') {
			ordersData.push(newOrder);
		}
	}

	// 2020
	ordersSheet2020.eachRow(row => getOrder(row));
	ordersSheet2025.eachRow(row => getOrder(row));

	return ordersData;
}

export default getDataOrders;
