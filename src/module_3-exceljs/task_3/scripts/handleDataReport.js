function handleDataReport(dirtyData) {
	const dirtyUsers = dirtyData.users;
	const dirtyProducts = dirtyData.products;
	const dirtyOrders = dirtyData.orders;

	const allProducts = dirtyProducts.length;
	const orders = dirtyOrders.length;
	const soldProducts = dirtyOrders.reduce(
		(acc, current) => acc + current.count,
		0
	);

	const dataProducts = [];
	dirtyOrders.forEach(order => {
		const productDataResult = dataProducts.find(
			dataItem => dataItem.id === order.idProduct
		);

		if (productDataResult) {
			productDataResult.count += 1;
		} else {
			const dataProduct = dirtyProducts.find(
				dirtyProduct => dirtyProduct.id === order.idProduct
			);

			dataProducts.push({ ...dataProduct, count: 0 });
		}
	});

	const topProducts = dataProducts
		.sort((a, b) => b.count - a.count)
		.map(product => `id=${product.id}, count=${product.count};`)
		.splice(0, 10)
		.join(' ');

	const dataUsers = [];

	dirtyOrders.forEach(order => {
		const userDataResult = dataUsers.find(
			dataItem => dataItem.id === order.idUser
		);

		if (userDataResult) {
			userDataResult.orders += 1;
		} else {
			const dataUser = dirtyUsers.find(
				dirtyUser => dirtyUser.id === order.idUser
			);

			dataUsers.push({ ...dataUser, orders: 0 });
		}
	});

	const topUsers = dataUsers
		.sort((a, b) => b.orders - a.orders)
		.map(product => `id=${product.id}, count=${product.orders};`)
		.splice(0, 10)
		.join(' ');

	return {
		allProducts,
		orders,
		soldProducts,
		topProducts,
		topUsers,
	};
}

export default handleDataReport;
