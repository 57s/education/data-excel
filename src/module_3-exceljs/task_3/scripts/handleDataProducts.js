import { PRODUCTS_REST } from '../../../data/products-rest.js';

function handleDataUsersProducts(dirtyData) {
	const dirtyProducts = dirtyData.products;
	const dirtyOrders = dirtyData.orders;
	const cleanProducts = [];

	dirtyProducts.forEach(dirtyProduct => {
		const restProductData = PRODUCTS_REST.find(
			restProduct => restProduct.id === dirtyProduct.id
		);

		const orderData = { totalSold: 0 };

		dirtyOrders.forEach(dirtyOrder => {
			if (dirtyOrder.idProduct === dirtyProduct.id) {
				orderData.totalSold += dirtyOrder.count;
			}
		});

		const newCleanProduct = {
			...dirtyProduct,
			...orderData,
			rest: restProductData.rest,
		};

		cleanProducts.push(newCleanProduct);
	});

	return cleanProducts.sort((a, b) => b.totalSold - a.totalSold);
}

export default handleDataUsersProducts;
