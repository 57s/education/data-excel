import ExcelJS from 'exceljs';

async function getDataProducts() {
	const productsBook = new ExcelJS.Workbook();
	const productsBookFile = await productsBook.xlsx.readFile(
		'./src/module_3-exceljs/task_1/output/products.xlsx'
	);

	const productsSheet = productsBookFile.getWorksheet('products');

	const productsData = [];

	productsSheet.eachRow(row => {
		const id = row.values[1];
		const name = row.values[2];
		const type = row.values[3];
		const price = row.values[4];

		const newProduct = {
			id,
			name,
			type,
			price,
		};

		// Фикс имени колонки
		if (newProduct.id !== 'Id') {
			productsData.push(newProduct);
		}
	});

	return productsData;
}

export default getDataProducts;
