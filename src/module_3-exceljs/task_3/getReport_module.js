import writeResult from './scripts/writeResult.js';

import getDataUsers from './scripts/getDataUsers.js';
import getDataProducts from './scripts/getDataProducts.js';
import getDataOrders from './scripts/getDataOrders.js';

import handleDataUsers from './scripts/handleDataUsers.js';
import handleDataUsersProducts from './scripts/handleDataProducts.js';
import handleDataOrders from './scripts/handleDataOrders.js';
import handleDataReport from './scripts/handleDataReport.js';

async function getReport() {
	const [userData, productData, ordersData] = await Promise.all([
		getDataUsers(),
		getDataProducts(),
		getDataOrders(),
	]);

	const dataDirty = {
		users: userData,
		products: productData,
		orders: ordersData,
	};

	const dataClean = {
		users: handleDataUsers(dataDirty),
		products: handleDataUsersProducts(dataDirty),
		orders: handleDataOrders(dataDirty),
		report: handleDataReport(dataDirty),
	};

	writeResult(dataClean);
}

getReport();
