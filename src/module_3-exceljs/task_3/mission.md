# Task 3

## Задание

- Название `report.xlsx`
- Данные брать из:
  - orders `./src/module_3-exceljs/task_1/output/orders.xlsx`
  - products - `./src/module_3-exceljs/task_1/output/products.xlsx`
  - users - `./src/module_3-exceljs/task_1/output/users.xlsx`

### Products

![Products](example/products.png)

- Лист `products`
- `totalSold` - продано всего
- `rest` - остаток на складе
- Отсортировать по `totalSold`

### Orders

![Orders](example/orders.png)

- Лист `orders`
- `'price',` - Цена за единицу товара
- `totalPrice` - Цена за заказ
- Отсортировать по `totalPrice`

### Users

![Users](example/users.png)

- Лист `users`
- `totalSpent` - всего потрачено
- `totalProducts` - всего куплено товаров
- `totalOrders` - заказов всего
- `orders` - массив с заказами
- Отсортировать по `totalSpent`

### Report

![Report](example/report.png)

- Лист `report`
- `allProducts` - Количество всех товаров
- `soldProducts` - Количество проданных товаров
- `orders` - Количество оформленных заказов
- `topProducts` - Топ самых популярных заказов
- `topUsers` - Топ самых самых заказывающих пользователей

### Decomposition

Разделить на функции

**Пример:**

- `getDataUsers` -  Собирает данные из книги
- `handleDataUsers` -  Обработка данных подготовка к записи
- `writeResult` - получает данные и записывает результат
- `getReport` - Точка входа в скрипт, запускает остальные модули
