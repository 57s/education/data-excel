import ExcelJS from 'exceljs';

import { PRODUCTS_REST } from '../../data/products-rest.js';

// Получить данные о пользователях
async function getDataUsers() {
	const usersBook = new ExcelJS.Workbook();
	const usersBookFile = await usersBook.xlsx.readFile(
		'./src/module_3-exceljs/task_1/output/users.xlsx'
	);

	const usersSheet = usersBookFile.getWorksheet('users');

	const usersData = [];

	usersSheet.eachRow(row => {
		const id = row.values[1];
		const name = row.values[2];
		const age = row.values[3];
		const city = row.values[4];

		const newUser = {
			id,
			name,
			age,
			city,
		};

		// Фикс имени колонки
		if (newUser.id !== 'Id') {
			usersData.push(newUser);
		}
	});

	return usersData;
}

// Получить данные о продуктах
async function getDataProducts() {
	const productsBook = new ExcelJS.Workbook();
	const productsBookFile = await productsBook.xlsx.readFile(
		'./src/module_3-exceljs/task_1/output/products.xlsx'
	);

	const productsSheet = productsBookFile.getWorksheet('products');

	const productsData = [];

	productsSheet.eachRow(row => {
		const id = row.values[1];
		const name = row.values[2];
		const type = row.values[3];
		const price = row.values[4];

		const newProduct = {
			id,
			name,
			type,
			price,
		};

		// Фикс имени колонки
		if (newProduct.id !== 'Id') {
			productsData.push(newProduct);
		}
	});

	return productsData;
}

// Получить данные о заказах
async function getDataOrders() {
	const ordersBook = new ExcelJS.Workbook();
	const ordersBookFile = await ordersBook.xlsx.readFile(
		'./src/module_3-exceljs/task_1/output/orders.xlsx'
	);

	const ordersSheet2020 = ordersBookFile.getWorksheet('2020');
	const ordersSheet2025 = ordersBookFile.getWorksheet('2025');

	const ordersData = [];

	function getOrder(row) {
		const id = row.values[1];
		const idUser = row.values[2];
		const idProduct = row.values[3];
		const count = row.values[4];

		const newOrder = {
			id,
			idUser,
			idProduct,
			count,
		};

		// Фикс имени колонки
		if (newOrder.id !== 'Id') {
			ordersData.push(newOrder);
		}
	}

	// 2020
	ordersSheet2020.eachRow(row => getOrder(row));
	ordersSheet2025.eachRow(row => getOrder(row));

	return ordersData;
}

// Обработать данные пользователей
function handleDataUsers(dirtyData) {
	const dirtyUsers = dirtyData.users;
	const dirtyOrders = dirtyData.orders;
	const dirtyProducts = dirtyData.products;
	const cleanUsers = [];

	dirtyUsers.forEach(dirtyUser => {
		// Инициализация дополнительных данных
		const orderData = {
			totalSpent: 0,
			totalProducts: 0,
			totalOrders: 0,
			orders: [],
		};

		// Сбор доп данных
		dirtyOrders.forEach(dirtyOrder => {
			if (dirtyUser.id === dirtyOrder.idUser) {
				const productData = dirtyProducts.find(
					productItem => productItem.id === dirtyOrder.idProduct
				);

				orderData.totalSpent += dirtyOrder.count * productData.price;
				orderData.totalProducts += dirtyOrder.count;
				orderData.totalOrders += 1;
				orderData.orders.push(dirtyOrder.id);
			}
		});

		const newCleanUser = {
			...dirtyUser,
			...orderData,
		};

		cleanUsers.push(newCleanUser);
	});

	return cleanUsers.sort((a, b) => b.totalSpent - a.totalSpent);
}

// Обработать данные продуктов
function handleDataProducts(dirtyData) {
	const dirtyProducts = dirtyData.products;
	const dirtyOrders = dirtyData.orders;
	const cleanProducts = [];

	dirtyProducts.forEach(dirtyProduct => {
		const restProductData = PRODUCTS_REST.find(
			restProduct => restProduct.id === dirtyProduct.id
		);

		const orderData = { totalSold: 0 };

		dirtyOrders.forEach(dirtyOrder => {
			if (dirtyOrder.idProduct === dirtyProduct.id) {
				orderData.totalSold += dirtyOrder.count;
			}
		});

		const newCleanProduct = {
			...dirtyProduct,
			...orderData,
			rest: restProductData.rest,
		};

		cleanProducts.push(newCleanProduct);
	});

	return cleanProducts.sort((a, b) => b.totalSold - a.totalSold);
}

// Обработать данные заказов
function handleDataOrders(dirtyData) {
	const dirtyOrders = dirtyData.orders;
	const dirtyProducts = dirtyData.products;
	const cleanOrders = [];

	dirtyOrders.forEach(dirtyOrder => {
		const productData = dirtyProducts.find(
			dirtyProduct => dirtyProduct.id === dirtyOrder.idProduct
		);

		const newCleanOrder = {
			...dirtyOrder,
			price: productData.price,
			totalPrice: dirtyOrder.count * productData.price,
		};

		cleanOrders.push(newCleanOrder);
	});

	return cleanOrders.sort((a, b) => b.totalPrice - a.totalPrice);
}

// Обработать данные отчета
function handleDataReport(dirtyData) {
	const dirtyUsers = dirtyData.users;
	const dirtyProducts = dirtyData.products;
	const dirtyOrders = dirtyData.orders;

	const allProducts = dirtyProducts.length;
	const orders = dirtyOrders.length;
	const soldProducts = dirtyOrders.reduce(
		(acc, current) => acc + current.count,
		0
	);

	const dataProducts = [];
	dirtyOrders.forEach(order => {
		const productDataResult = dataProducts.find(
			dataItem => dataItem.id === order.idProduct
		);

		if (productDataResult) {
			productDataResult.count += 1;
		} else {
			const dataProduct = dirtyProducts.find(
				dirtyProduct => dirtyProduct.id === order.idProduct
			);

			dataProducts.push({ ...dataProduct, count: 0 });
		}
	});

	const topProducts = dataProducts
		.sort((a, b) => b.count - a.count)
		.map(product => `id=${product.id}, count=${product.count};`)
		.splice(0, 10)
		.join(' ');

	const dataUsers = [];

	dirtyOrders.forEach(order => {
		const userDataResult = dataUsers.find(
			dataItem => dataItem.id === order.idUser
		);

		if (userDataResult) {
			userDataResult.orders += 1;
		} else {
			const dataUser = dirtyUsers.find(
				dirtyUser => dirtyUser.id === order.idUser
			);

			dataUsers.push({ ...dataUser, orders: 0 });
		}
	});

	const topUsers = dataUsers
		.sort((a, b) => b.orders - a.orders)
		.map(product => `id=${product.id}, count=${product.orders};`)
		.splice(0, 10)
		.join(' ');

	return {
		allProducts,
		orders,
		soldProducts,
		topProducts,
		topUsers,
	};
}

// Записать результат
function writeResult(data) {
	// Создать книгу
	const reportBook = new ExcelJS.Workbook();

	// Записать лист с отчетом
	function writeReport(dataReport) {
		// Перейти на лист
		const reportSheet = reportBook.addWorksheet('report');
		// Настроить заголовки
		reportSheet.columns = [
			{ header: 'metrics', key: 'metrics', width: 15 },
			{ header: 'Data', key: 'data', width: 100 },
		];

		// Заполнение книги
		Object.entries(dataReport).forEach(([metrics, data]) => {
			reportSheet.addRow({
				metrics,
				data,
			});
		});
	}

	// Записать лист с пользователями
	function writeUsers(dataUsers) {
		// Перейти на лист
		const usersSheet = reportBook.addWorksheet('users');

		// Настроить заголовки
		usersSheet.columns = [
			{ header: 'Id', key: 'id', width: 10 },
			{ header: 'Name', key: 'name', width: 10 },
			{ header: 'Age', key: 'age', width: 5 },
			{ header: 'City', key: 'city', width: 20 },
			{ header: 'Total spent', key: 'totalSpent', width: 20 },
			{ header: 'Total products', key: 'totalProducts', width: 20 },
			{ header: 'Total orders', key: 'totalOrders', width: 20 },
			{ header: 'orders', key: 'orders', width: 150 },
		];

		// Заполнение листа
		dataUsers.forEach(user => {
			usersSheet.addRow(user);
		});
	}

	// Записать лист с продуктами
	function writeProduct(dataProducts) {
		// Перейти на лист
		const productSheet = reportBook.addWorksheet('product');

		// Настроить заголовки
		productSheet.columns = [
			{ header: 'Id', key: 'id', width: 10 },
			{ header: 'Name', key: 'name', width: 25 },
			{ header: 'Type', key: 'type', width: 15 },
			{ header: 'Price', key: 'price', width: 10 },
			{ header: 'Total sold', key: 'totalSold', width: 15 },
			{ header: 'Rest', key: 'rest', width: 7 },
		];

		// Заполнение листа
		dataProducts.forEach(product => {
			productSheet.addRow(product);
		});
	}

	// Записать лист с заказами
	function writeOrders(dataOrders) {
		// Перейти на лист
		const ordersSheet = reportBook.addWorksheet('orders');

		// Настроить заголовки
		ordersSheet.columns = [
			{ header: 'Id', key: 'id', width: 10 },
			{ header: 'Id user', key: 'idUser', width: 10 },
			{ header: 'id product', key: 'idProduct', width: 10 },
			{ header: 'Count', key: 'count', width: 7 },
			{ header: 'Price', key: 'price', width: 10 },
			{ header: 'TotalPrice', key: 'totalPrice', width: 10 },
		];

		// Заполнение листа
		dataOrders.forEach(order => {
			ordersSheet.addRow(order);
		});
	}

	writeReport(data.report);
	writeUsers(data.users);
	writeProduct(data.products);
	writeOrders(data.orders);

	// Записать файл с результатом
	reportBook.xlsx.writeFile('./src/module_3-exceljs/task_3/output/report.xlsx');
}

async function getReport() {
	const userData = await getDataUsers();
	const productData = await getDataProducts();
	const ordersData = await getDataOrders();

	const dataDirty = {
		users: userData,
		products: productData,
		orders: ordersData,
	};

	const dataClean = {
		users: handleDataUsers(dataDirty),
		products: handleDataProducts(dataDirty),
		orders: handleDataOrders(dataDirty),
		report: handleDataReport(dataDirty),
	};

	writeResult(dataClean);
}

getReport();
