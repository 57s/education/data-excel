# Warm-up

- Прочитать книгу `./src/module_3-exceljs/task_0/input/testBook.xlsx`

## Task 1

- пройтись по строкам и вывести имена

## Task 2

- пройтись по столбцу `Full price` и вывести его в консоль

## Task 3

- Получить `Total count(G3)` и вывести в консоль
