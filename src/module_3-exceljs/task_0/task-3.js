import ExcelJS from 'exceljs';

const workBook = new ExcelJS.Workbook();

workBook.xlsx.readFile('./src/module_3-exceljs/task_0/input/testBook.xlsx').then(() => {
	const worksheet = workBook.getWorksheet(1);
	const fullCount = worksheet.getCell('G3').value.result;
	const fullPrice = worksheet.getCell('H3').value.result;
	console.log(`Всего Count ${fullCount}`);
	console.log(`Всего Price ${fullPrice}`);
});
