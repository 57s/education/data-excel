import ExcelJS from 'exceljs';

const workBook = new ExcelJS.Workbook();

workBook.xlsx.readFile('./src/module_3-exceljs/task_0/input/testBook.xlsx').then(() => {
	const worksheet = workBook.getWorksheet(1);
	worksheet.eachRow(row => {
		console.log(JSON.stringify(row.values[2]));
	});
});
