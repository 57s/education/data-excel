import ExcelJS from 'exceljs';

const workBook = new ExcelJS.Workbook();

workBook.xlsx.readFile('./src/module_3-exceljs/task_0/input/testBook.xlsx').then(() => {
	const worksheet = workBook.getWorksheet(1);
	const columnE = worksheet.getColumn('E');
	columnE.eachCell(cell => {
		const result = cell.value.result;
		if (result) {
			console.log(result);
		}
	});
});
