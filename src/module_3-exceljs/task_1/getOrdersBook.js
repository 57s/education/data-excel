import ExcelJS from 'exceljs';

import { ORDERS2020 } from '../../data/orders2020.js';
import { ORDERS2025 } from '../../data/orders2025.js';

async function getOrdersBook() {
	// Создать книгу
	const ordersBook = new ExcelJS.Workbook();

	// создать листы
	const orders2020Sheet = ordersBook.addWorksheet('2020');
	const orders2025Sheet = ordersBook.addWorksheet('2025');

	// Настроить заголовки
	orders2020Sheet.columns = [
		{ header: 'Id', key: 'id', width: 10 },
		{ header: 'Id User', key: 'idUser', width: 10 },
		{ header: 'Id Product', key: 'idProduct', width: 10 },
		{ header: 'Count', key: 'count', width: 5 },
	];

	orders2025Sheet.columns = [
		{ header: 'Id', key: 'id', width: 10 },
		{ header: 'Id User', key: 'idUser', width: 10 },
		{ header: 'Id Product', key: 'idProduct', width: 10 },
		{ header: 'Count', key: 'count', width: 5 },
	];

	// Заполнение книги
	ORDERS2020.forEach(order => {
		orders2020Sheet.addRow(order);
	});

	ORDERS2025.forEach(order => {
		orders2025Sheet.addRow(order);
	});

	ordersBook.xlsx.writeFile('./src/module_3-exceljs/task_1/output/orders.xlsx');
}

export default getOrdersBook;
