import ExcelJS from 'exceljs';

import { USERS } from '../../data/users.js';

async function getUsersBook() {
	// Создать книгу
	const usersBook = new ExcelJS.Workbook();

	// Перейти на лист
	const usersSheet = usersBook.addWorksheet('users');

	// Настроить заголовки
	usersSheet.columns = [
		{ header: 'Id', key: 'id', width: 10 },
		{ header: 'Name', key: 'name', width: 10 },
		{ header: 'Age', key: 'age', width: 5 },
		{ header: 'City', key: 'city', width: 20 },
	];

	// Заполнение книги
	USERS.forEach(user => {
		usersSheet.addRow(user);
	});

	usersBook.xlsx.writeFile('./src/module_3-exceljs/task_1/output/users.xlsx');
}

export default getUsersBook;
