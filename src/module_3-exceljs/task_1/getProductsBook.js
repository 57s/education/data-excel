import ExcelJS from 'exceljs';

import { PRODUCTS } from '../../data/products.js';

async function getProductsBook() {
	// Создать книгу
	const productsBook = new ExcelJS.Workbook();

	// Перейти на лист
	const productsSheet = productsBook.addWorksheet('products');

	// Настройка стиля шрифта колонок
	const headerFontStyle = {
		italic: true, // Курсивный шрифт
		size: 12,
	};

	// Настройка выравнивания для колонок
	const headerAlignment = {
		vertical: 'middle',
		horizontal: 'center',
	};

	// Настроить заголовки
	productsSheet.columns = [
		{
			header: 'Id',
			key: 'id',
			width: 10,
			style: { font: headerFontStyle, alignment: headerAlignment },
		},
		{
			header: 'Label',
			key: 'label',
			width: 30,
			style: { font: headerFontStyle, alignment: headerAlignment },
		},
		{
			header: 'Category',
			key: 'category',
			width: 15,
			style: { font: headerFontStyle, alignment: headerAlignment },
		},
		{
			header: 'Price',
			key: 'price',
			width: 10,
			style: { font: headerFontStyle, alignment: headerAlignment },
		},
	];

	const idProductsSheet = productsSheet.getColumn('id');
	const labelProductsSheet = productsSheet.getColumn('label');
	const categoryProductsSheet = productsSheet.getColumn('category');
	const priceProductsSheet = productsSheet.getColumn('price');

	// Задать колонке `id` фоновый цвет
	idProductsSheet.fill = {
		type: 'pattern',
		pattern: 'solid',
		fgColor: { argb: 'FFB6C1' },
	};

	// Задать колонке `label` фоновый цвет
	labelProductsSheet.fill = {
		type: 'pattern',
		pattern: 'solid',
		fgColor: { argb: 'FFDAB9' },
	};

	// Задать колонке `category` фоновый цвет
	categoryProductsSheet.fill = {
		type: 'pattern',
		pattern: 'solid',
		fgColor: { argb: 'ADD8E6' },
	};

	// Задать колонке `price` фоновый цвет
	priceProductsSheet.fill = {
		type: 'pattern',
		pattern: 'solid',
		fgColor: { argb: '98FB98' },
	};

	// Стилизация заголовков
	productsSheet.getRow(1).font = {
		bold: true,
		size: 14,
		color: { argb: 'FFFFFF ' },
	};

	// Задать колонке заголовкам фоновый цвет
	['A1', 'B1', 'C1', 'D1'].forEach(key => {
		productsSheet.getCell(key).fill = {
			type: 'pattern',
			pattern: 'solid',
			fgColor: { argb: '808080' },
		};
	});

	// Заполнение книги
	PRODUCTS.forEach(product => {
		// Сопоставление свойств
		const newRow = {
			id: product.id,
			label: product.name,
			category: product.type,
			price: product.price,
		};

		productsSheet.addRow(newRow);
	});

	productsBook.xlsx.writeFile('./src/module_3-exceljs/task_1/output/products.xlsx');
}

getProductsBook();

export default getProductsBook;
