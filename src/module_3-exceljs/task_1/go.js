import getUsersBook from './getUsersBook.js';
import getProductsBook from './getProductsBook.js';
import getOrdersBook from './getOrdersBook.js';

getUsersBook();
getProductsBook();
getOrdersBook();
