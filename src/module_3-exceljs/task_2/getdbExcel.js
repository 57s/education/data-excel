import ExcelJS from 'exceljs';

import { USERS } from '../../data/users.js';
import { PRODUCTS } from '../../data/products.js';
import { PRODUCTS_REST } from '../../data/products-rest.js';
import { ORDERS2020 } from '../../data/orders2020.js';
import { ORDERS2025 } from '../../data/orders2025.js';

// Обработка данных пользователей
function usersHandler() {
	const usersResult = [];

	[...ORDERS2020, ...ORDERS2025].forEach(orderElement => {
		const userResult = usersResult.find(
			userItem => userItem.id === orderElement.idUser
		);

		if (userResult) {
			// Если пользователь уже в результате
			//  то суммируем
			userResult.orders++;
			userResult.products += orderElement.count;
		} else {
			// Если пользователя нет в результате
			// То пушим его туда
			// Добавив новые данные

			const userData = USERS.find(
				userElement => userElement.id === orderElement.idUser
			);

			usersResult.push({
				...userData,
				orders: 1,
				products: orderElement.count,
			});
		}
	});

	// Сортировка результата
	return usersResult.sort((a, b) => b.products - a.products);
}

// Обработка данных продуктов
function productsHandler() {
	const productsResult = PRODUCTS.map(productElement => {
		//  Получить остаток продуктов на складе
		const restProductData = PRODUCTS_REST.find(
			productRestItem => productRestItem.id === productElement.id
		);

		// Инициализация дополнительный свойств
		const updateDataProduct = {
			sold: 0,
			money: 0,
			rest: restProductData.rest,
		};

		// Собрать количество и цену
		[...ORDERS2020, ...ORDERS2025].forEach(orderElement => {
			if (orderElement.idProduct === productElement.id) {
				updateDataProduct.sold += orderElement.count;
				updateDataProduct.money += productElement.price * orderElement.count;
			}
		});

		return {
			...productElement,
			...updateDataProduct,
		};
	});

	// Сортировка
	return productsResult.sort((a, b) => b.rest - a.rest);
}

// Обработка данных заказов
function ordersHandler() {
	const ordersResult = [...ORDERS2020, ...ORDERS2025].map(ordersElement => {
		const product = PRODUCTS.find(
			productItem => productItem.id === ordersElement.idProduct
		);

		// Дополнение новой информацией
		return {
			...ordersElement,
			price: product.price,
			totalPrice: product.price * ordersElement.count,
		};
	});

	// Сортировка
	return ordersResult.sort((a, b) => b.totalPrice - a.totalPrice);
}

async function getDBBook() {
	// Создать книгу
	const dbBook = new ExcelJS.Workbook();

	// создать листы
	const usersSheet = dbBook.addWorksheet('users');
	const productsSheet = dbBook.addWorksheet('products');
	const ordersSheet = dbBook.addWorksheet('orders');

	// Настройки заголовков для users
	usersSheet.columns = [
		{ header: 'Id', key: 'id', width: 10 },
		{ header: 'Name', key: 'name', width: 10 },
		{ header: 'Age', key: 'age', width: 5 },
		{ header: 'City', key: 'city', width: 20 },
		{ header: 'Orders', key: 'orders', width: 7 },
		{ header: 'Products', key: 'products', width: 7 },
	];

	// Настройки заголовков для products
	productsSheet.columns = [
		{ header: 'Id', key: 'id', width: 10 },
		{ header: 'Name', key: 'name', width: 30 },
		{ header: 'Type', key: 'type', width: 7 },
		{ header: 'Price', key: 'price', width: 10 },
		{ header: 'Sold', key: 'sold', width: 10 },
		{ header: 'Money', key: 'money', width: 10 },
		{ header: 'Rest', key: 'rest', width: 10 },
	];

	// Настройки заголовков для orders
	ordersSheet.columns = [
		{ header: 'Id', key: 'id', width: 10 },
		{ header: 'Id User', key: 'idUser', width: 10 },
		{ header: 'Id Product', key: 'idProduct', width: 10 },
		{ header: 'Count', key: 'count', width: 7 },
		{ header: 'Price', key: 'price', width: 10 },
		{ header: 'Total price', key: 'totalPrice', width: 10 },
	];

	// Заполнение листа с users
	const usersData = usersHandler();
	usersData.forEach(user => {
		usersSheet.addRow(user);
	});

	// Заполнение листа с products
	const productsData = productsHandler();
	productsData.forEach(product => {
		productsSheet.addRow(product);
	});

	// Заполнение листа с products
	const ordersData = ordersHandler();
	ordersData.forEach(order => {
		ordersSheet.addRow(order);
	});

	dbBook.xlsx.writeFile('./src/module_3-exceljs/task_2/output/dbExcel.xlsx');
}

getDBBook();
