# Task 2

## Задание

- Название `dbExcel.xlsx`

### Orders

![Orders image](example/orders.png)

- Лист `orders`
- Объединить `ORDERS2020` и `ORDERS2025`
- Добавить `price` - цена за единицу товара
- Добавить `totalPrice` - цена заказа(price \* count)
- Отсортировать по `totalPrice`

### Users

![Alt text](example/users.png)

- Лист `users`
- Добавить `orders` - количество сделанных заказов
- Добавить`product` - количество купленных товаров
- Отсортировать по `product`

### Products

![Alt text](example/product.png)

- Лист `products`
- Добавить `sold` - Количество проданных экземпляров
- Добавить `money` - Заработано денег
- Добавить `rest` из `PRODUCTS_REST ` - Осталось на складе
- Отсортировать по `rest`
