export const checkPlatform = () => {
	if (typeof window !== 'undefined') {
		return 'BROWSER';
	}
	return 'NODE';
};
