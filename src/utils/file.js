import fs from 'fs/promises';

export const checkFileExists = async (filePath) => {
	return new Promise(async (resolve, reject) => {
		try {
			await fs.access(filePath);
			resolve(true);
		} catch (err) {
			resolve(false);
		}
	});
};

export const writeFileJson = (filePath, data) =>
	fs.writeFile(filePath, JSON.stringify(data), 'utf8');

export const getFileJson = async (filePath, defaultData) => {
	const isFile = await checkFileExists(filePath);

	return new Promise(async (resolve) => {
		if (isFile) {
			const fileContent = await fs.readFile(filePath, 'utf8');
			resolve(JSON.parse(fileContent));
		} else {
			writeFileJson(filePath, defaultData);
			resolve(defaultData);
		}
	});
};
