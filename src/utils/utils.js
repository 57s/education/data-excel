export const getRandomNumber = (min = 0, max = 1_000) => {
	return Math.floor(Math.random() * (max - min + 1)) + min;
};
