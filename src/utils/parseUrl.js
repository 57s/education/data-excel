export const parseUrl = (url) => {
	try {
		const urlObject = new URL(url);
		return {
			protocol: urlObject.protocol,
			hostname: urlObject.hostname,
			pathname: urlObject.pathname,
			search: urlObject.search,
			hash: urlObject.hash,
		};
	} catch (error) {
		console.error('Invalid URL:', error.message);
		return null;
	}
};
