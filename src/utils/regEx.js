export const regExUsersAll = (path) => {
	const regex = /^\/users$/;
	return regex.test(path);
};

export const regExUsersId = (path) => {
	const regex = /^\/users\/u\d+$/;
	return regex.test(path);
};
