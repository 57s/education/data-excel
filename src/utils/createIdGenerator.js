export const createIdGenerator = (prefix, pad = 5) => {
	let count = 1;

	return () => `${prefix}${String(count++).padStart(pad, '0')}`;
};
