import { createIdGenerator } from './createIdGenerator.js';

export const getIdUser = createIdGenerator('u');
export const getIdOrder2020 = createIdGenerator('o2020_');
export const getIdOrder2025 = createIdGenerator('o2025_');
export const getIdProduct = createIdGenerator('p');
