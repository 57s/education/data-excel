import { getRandomNumber } from './utils.js';
import { wait } from './wait.js';

import { SERVER_RANDOM_ERRORS } from '../server/constants/serverRandomErrors.js';
import { createAnswer } from '../server/helpers/createAnswer.js';
import { SERVER_CODES } from '../server/constants/serverCodes.js';

const defaultConfig = {
	throttledMsMin: 500,
	throttledMsMax: 1500,
	probabilityError: 0,
};

export const getServerSimulation = (props) => {
	const startTime = performance.now();
	const { getData, config: customConfig = {}, callback } = props;

	const config = {
		...defaultConfig,
		...customConfig,
	};

	const throttledMs = getRandomNumber(
		config.throttledMsMin,
		config.throttledMsMax
	);

	const isError = getRandomNumber(1, 1000) <= 1000 * config.probabilityError;

	return new Promise(async (resolve, reject) => {
		const getResponse = (answer) => {
			if (!isError) {
				const endTime = performance.now();
				const response = {
					...answer,
					throttledMs: endTime - startTime,
				};

				if (callback) callback(response);
				resolve(response);
			} else {
				const errorIndex = getRandomNumber(0, SERVER_RANDOM_ERRORS.length - 1);
				const endTime = performance.now();

				const response = createAnswer({
					ok: false,
					status: SERVER_CODES.notAvailable.message,
					code: SERVER_CODES.notAvailable.code,
					error: SERVER_RANDOM_ERRORS[errorIndex],
					throttledMs: endTime - startTime,
				});

				if (callback) callback(response);
				reject(response);
			}
		};

		await wait(throttledMs);

		try {
			const answer = await getData();
			getResponse(answer);
		} catch (err) {
			if (callback) callback(err);
			reject(err);
		}
	});
};
