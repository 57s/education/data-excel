const urlBase = 'https://jsonplaceholder.typicode.com';
const urlTodo = `${urlBase}/todos`;
const urlTodo1 = `${urlTodo}/1`;

fetch(urlTodo1)
	.then((response) => response.json())
	.then(console.log);
