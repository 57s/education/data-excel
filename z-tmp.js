import { getRandomNumber } from './src/utils/utils.js';

const f1 = () =>
	new Promise((resolve, reject) => {
		setTimeout(() => {
			const randomNumber = getRandomNumber();
			if (randomNumber > 500) {
				resolve({ ok: true, message: 'f1' });
			} else {
				reject({ ok: false, message: 'f1' });
			}
		}, 1000);
	});

const f2 = () =>
	new Promise((resolve, reject) => {
		setTimeout(async () => {
			const randomNumber = getRandomNumber();

			try {
				const f1Result = await f1();

				if (randomNumber > 500) {
					const f2Result = { ok: true, message: 'f2', f1: f1Result };
					resolve(f2Result);
				} else {
					const f2Result = { ok: false, message: 'f2', f1: f1Result };
					reject(f2Result);
				}
			} catch (err) {
				reject(err);
			}
		}, 1000);
	});

const wrap = async () => {
	try {
		const f2Itog = await f2();
		console.group('try');
		console.log(f2Itog);
	} catch (error) {
		console.group('catch');
		console.log(error);
	}
};

wrap();
