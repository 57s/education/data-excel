# Async

## Async/await

Синтаксический сахар, который позволяет писать асинхронный код в стиле синхронного кода.

- `async` - Указывает что функция будут асинхронной
- `await` - Указывает что нужно дождаться выполнения промиса

### Объявление асинхронных функций

**Arrow function**

```js
async () => {
	const result = await promise;
	console.log(result);
};
```

**Function expression**

```js
const myFunction = async function () {
	const result = await promise;
	console.log(result);
};
```

**Function declaration**

```js
async function myAsyncFunction() {
	const result = await promise;
	console.log(result);
}
```

### Пример

В этом примере мы используем метод `readFile` для чтения файла Excel
и методы `eachSheet` и `eachRow` для обхода всех строк в каждом листе.

```js
const ExcelJS = require('exceljs');

async function readExcel() {
	const workbook = new ExcelJS.Workbook();

	await workbook.xlsx.readFile('test.xlsx');

	workbook.eachSheet(async (worksheet, sheetId) => {
		console.log(`Sheet ${sheetId}`);

		worksheet.eachRow(async (row, rowNumber) => {
			console.log(`Row ${rowNumber} = ${JSON.stringify(row.values)}`);
		});
	});
}
```

## Then

Один из способов асинхронного выполнение кода

### Пример

В этом примере мы используем метод `readFile` для чтения файла Excel
и методы `eachSheet` и `eachRow` для обхода всех строк в каждом листе.

```js
const ExcelJS = require('exceljs');

function readExcel() {
	const workbook = new ExcelJS.Workbook();

	workbook.xlsx.readFile('test.xlsx').then(() => {
		workbook.eachSheet((worksheet, sheetId) => {
			console.log(`Sheet ${sheetId}`);

			worksheet.eachRow((row, rowNumber) => {
				console.log(`Row ${rowNumber} = ${JSON.stringify(row.values)}`);
			});
		});
	});
}
```

## Параллельное выполнение

Метод `Promise.all()` возвращает массив с результатами выполнения всех переданных ему промисов.
Метод `Promise.all()` запускает промисы параллельно и дожидается выполнения всех промисов, после возвращает результат.

- Результат выполнения `cleanDataPlaces(dirtyDataPlaces)` попадет в `dataPlaces`
- Результат выполнения `cleanDataExport(dirtyDataExport)` попадет в `dataExport`

```js
const [dataPlaces, dataExport] = await Promise.all([
	cleanDataPlaces(dirtyDataPlaces),
	cleanDataExport(dirtyDataExport),
]);
```
