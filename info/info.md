# Info

- `exceljs` библиотека для работы с Microsoft Excel

## Commands

```shell
npm i exceljs
```

## Doc

- [git](https://github.com/exceljs/exceljs)
- [builtin](https://builtin.com/software-engineering-perspectives/exceljs)
