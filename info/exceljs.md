# Exceljs

## Создать книгу

```js
const workBook = new ExcelJS.Workbook();
```

## Прочитать книгу

```js
const workBookFile = await workBook.xlsx.readFile('./input/book.xlsx');
```

## Записать книгу

```js
workBookFile.xlsx.writeFile(`./output/tableResult_${timestamp}.xlsx`);
```

## Перейти на лист

```js
const workSheet = workBookFile.getWorksheet(1);
```

## Добавить лист

```js
const workSheet = workBookFile.addWorksheet('workSheet');
```

## Получить колонку

```js
const workColumn = worksheet.getColumn(column);
```

## Сконфигурировать заголовки колонок

```js
workSheet.columns = [
	{ header: 'Client Id ', key: 'clientId', width: 15 },
	{ header: 'EAN', key: 'ean', width: 40 },
];
```

## Проитерироваться по листам

```js
workbook.eachSheet((worksheet, sheetId) => {
	console.log(`Sheet ${sheetId}`);
});
```

## Проитерироваться по строкам

```js
worksheet.eachRow((row, rowNumber) => {
	console.log(`Row ${rowNumber}`);
});
```

## Проитерироваться по столбцу

```js
workColumn.eachCell((cell, rowNumber) => {
	console.log(`Cell ${cell.address} = ${cell.value}`);
});
```

## Добавить строку

```js
worksheet.addRow({
	id,
	name,
});
```

## Добавить в ячейку

```js
worksheet.getCell('A1').value = 'Hello, World!';
```

## Получить значение ячейки строки

```js
row.values[1];
```

## Кастомизировать внешний вид колонки

```js
workColumn.fill = {
	type: 'pattern',
	pattern: 'solid',
	fgColor: { argb: 'FFB6C1' },
};
```

## Кастомизировать внешний вид колонок

```js
// Настройка стиля шрифта колонок
const headerFontStyle = {
	italic: true, // Курсивный шрифт
	size: 12, // Размер шрифта
};

// Настройка выравнивания для колонок
const headerAlignment = {
	vertical: 'middle',
	horizontal: 'center',
};

// Настроить заголовки
productsSheet.columns = [
	{
		header: 'Id',
		key: 'id',
		width: 10,
		style: { font: headerFontStyle, alignment: headerAlignment },
	},
];
```

## Кастомизировать внешний вид ячеек

```js
// Задать колонке заголовкам фоновый цвет
['A1', 'B1', 'C1', 'D1'].forEach(key => {
	productsSheet.getCell(key).fill = {
		type: 'pattern',
		pattern: 'solid',
		fgColor: { argb: '808080' },
	};
});
```
