import { USERS } from '../../../../src/data/users.js';
import { getServerSimulationPromise } from '../../../utils/getServerSimulationPromise.js';

export const getUserById = (id) => {
	const user = USERS.find((userItem) => userItem.id === id);
	return getServerSimulationPromise(user);
};

export const getAllUsers = () => {
	return getServerSimulationPromise(USERS);
};
