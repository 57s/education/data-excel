import { ORDERS2020 } from '../../../data/orders2020.js';
import { ORDERS2025 } from '../../../data/orders2025.js';
import { getServerSimulationPromise } from '../../../utils/getServerSimulationPromise.js';

export const getOrder2020ById = (id) => {
	const order = ORDERS2020.find((orderItem) => orderItem.id === id);
	return getServerSimulationPromise(order);
};

export const getAllOrder2020 = () => {
	return getServerSimulationPromise(ORDERS2020);
};

export const getOrder2025ById = (id) => {
	const order = ORDERS2025.find((orderItem) => orderItem.id === id);
	return getServerSimulationPromise(order);
};

export const getAllOrder2025 = () => {
	return getServerSimulationPromise(ORDERS2025);
};
