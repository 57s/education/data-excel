import { PRODUCTS } from '../../../data/products.js';
import { PRODUCTS_REST } from '../../../data/products-rest.js';
import { getServerSimulationPromise } from '../../../utils/getServerSimulationPromise.js';

export const getProductById = (id) => {
	const product = PRODUCTS.find((productItem) => productItem.id === id);
	return getServerSimulationPromise(product);
};

export const getAllProduct = () => {
	return getServerSimulationPromise(PRODUCTS);
};

export const getRestAllProduct = () => {
	return getServerSimulationPromise(PRODUCTS_REST);
};
