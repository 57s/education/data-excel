# Read me

## Необходимые базовые знания

- `reduce`
- `map`
- `foreach`
- `filter`
- `find`
- `sort`
- `localeCompare`
- `push`
- `slice`
- `destructuring`
- `if`
- `switch case`

- `Promises`
- `Async/await`
- `.then()`
